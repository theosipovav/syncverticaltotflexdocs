﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyncVerticalToTFlexDocs
{
    class AppCore
    {
        private AppAsconVertical appAsconVertical;
        private AppTflex appTflex;
        private AppData appData;

        public AppCore()
        {
            //
        }




        /// <summary>
        /// Импорт объектов для справочника "Режущий инструмент" из базы данных системы АСКОН
        /// </summary>
        public void importCT()
        {
            string shortname = "РИ";
            string longname = "Режущий инструмент";
            string textError = "";
            int countItemsAll = 0;
            updateLog(String.Format("Загрузка объектов <{0}> из базы данных Вертикаль.", longname));
            List<Dictionary<string, string>> listLoadObjects = appAsconVertical.getCT();
            updateLog("Загружено " + listLoadObjects.Count + " объектов.");
            updateLog("Загрузка динамических параметров для объектов режущих инструментов из базы данных Вертикаль.");
            updateProgresBarPre(0, listLoadObjects.Count, 0, listLoadObjects.Count);
            List<DinamicParam> listDinamicParams = new List<DinamicParam>();
            for (int i = 0; i < listLoadObjects.Count; i++)
            {
                updateProgresBarPre(i, listLoadObjects.Count, 0, listLoadObjects.Count);
                string guid = listLoadObjects[i]["item_GUID"];
                string fguid = listLoadObjects[i]["item_FGUID"];
                List<Dictionary<string, string>> p = appAsconVertical.getDinamicParam(guid, fguid);
                DinamicParam dinamicParam = new DinamicParam(guid, fguid, p);
                listDinamicParams.Add(dinamicParam);

            }
            updateLog("Синхронизация объектов T-Flex DOCs.");
            // Сортировка по виду инструмента
            foreach (ICollection<Dictionary<string, string>> groupClass in listLoadObjects.GroupBy(x => x["class_GUID"]))
            {
                string nameGroupClass = appData.replaceNameParameterVertical(groupClass.First()["class_NAME"].Trim(' '), shortname);
                string guidGroupClass = groupClass.First()["class_GUID"];
                string guidClass = appData.getGuidTool(shortname, shortname, "Группа");
                string tagGroup = "class_";
                try
                {
                    Dictionary<string, string> parametersGroupClass = getParametersForGroup(groupClass.First(), tagGroup, shortname, nameGroupClass, null);
                    ReferenceObject folderClass = appTflex.createObjectCuttingTool(guidClass, parametersGroupClass);
                    // Сортировка по типу РИ
                    foreach (ICollection<Dictionary<string, string>> group in groupClass.GroupBy(x => x["group_GUID"]))
                    {
                        string nameGroup = group.First()["group_GROUPRI"].Trim(' ');
                        string guidGroup = group.First()["group_GUID"];
                        guidClass = appData.getGuidTool(shortname, shortname, "Группа");
                        tagGroup = "group_";
                        try
                        {
                            Dictionary<string, string> parametersGroup = getParametersForGroup(group.First(), "group_", shortname, nameGroup, parametersGroupClass);
                            foreach (KeyValuePair<string, string> parameter in parametersGroupClass)
                            {
                                if (!parametersGroup.ContainsKey(parameter.Key))
                                {
                                    parametersGroup.Add(parameter.Key, parameter.Value);
                                }
                            }
                            ReferenceObject folder = appTflex.createObjectCuttingTool(guidClass, parametersGroup, folderClass);
                            //
                            // Сортировка по РИ
                            foreach (ICollection<Dictionary<string, string>> group2 in group.GroupBy(x => x["group2_GUID"]))
                            {
                                string nameGroup2 = group2.First()["group2_NAME_RI"].Trim(' ');
                                string guidGroup2 = group2.First()["group2_GUID"];
                                tagGroup = "group2_";
                                try
                                {
                                    // Создание папки "Наименование группы РИ+Стандарт" как дочернию для папки "Наименование типа РИ"
                                    guidClass = appData.getGuidTool(shortname, shortname, "Группа");
                                    Dictionary<string, string> parametersGroup2 = getParametersForGroup(group2.First(), "group2_", shortname, nameGroup2, parametersGroup);
                                    ReferenceObject folder2 = appTflex.createObjectCuttingTool(guidClass, parametersGroup2, folder);
                                    updateLog(String.Format("Синхронизация {0} > {1} > {2}: {3} объектов.", nameGroupClass, nameGroup, nameGroup2, group2.Count()));
                                    int countItems = 0;
                                    foreach (Dictionary<string, string> groupItems in group2)
                                    {
                                        string nameGroupItem = groupItems["item_NAME"].Trim(' '); ;
                                        string guidGroupItem = groupItems["item_GUID"];
                                        guidClass = appData.getGuidTool(shortname, shortname, "Экземпляр");
                                        tagGroup = "item_";
                                        Dictionary<string, string> paramsItems = new Dictionary<string, string>();
                                        try
                                        {
                                            guidClass = appData.getGuidTool(shortname, shortname, "Экземпляр");
                                            paramsItems = getParametersForGroup(groupItems, "item_", shortname, nameGroupItem, parametersGroup2);
                                            foreach (DinamicParam param in listDinamicParams.Where(x => x.guid == groupItems["item_GUID"] && x.fguid == groupItems["item_FGUID"]))
                                            {
                                                foreach (KeyValuePair<string, string> parameterItem in param.parameters)
                                                {
                                                    string name = parameterItem.Key;
                                                    string value = AppData.replaceChars(parameterItem.Value);
                                                    paramsItems.Add(name, value);
                                                }
                                            }
                                            ReferenceObject obj = appTflex.createObjectCuttingTool(guidClass, paramsItems, folder2);
                                            updateProgresBarPre(countItems++, group2.Count(), countItemsAll++, listLoadObjects.Count);
                                        }
                                        catch (Exception ex)
                                        {
                                            System.Diagnostics.Debugger.Break();
                                            textError += String.Format("{0} > {1} > {2} -> {3}", nameGroupClass, nameGroup, nameGroup2, nameGroupItem);
                                            textError += String.Format("\n{0}", ex.Message);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    System.Diagnostics.Debugger.Break();
                                    textError += String.Format("{0} > {1} > {2}", nameGroupClass, nameGroup, nameGroup2);
                                    textError += String.Format("\n{0}", ex.Message);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            System.Diagnostics.Debugger.Break();
                            textError += String.Format("{0} > {1}", nameGroupClass, nameGroup);
                            textError += String.Format("\n{0}", ex.Message);
                        }
                    }
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debugger.Break();
                    textError += String.Format("{0}", nameGroupClass);
                    textError += String.Format("\n{0}", ex.Message);
                }
            }
            updateLog("Синхронизация завершена");
            if (textError != "")
            {
                updateLog("...");
                updateLog("При синхронизации были обнаружены ошибки:");
                updateLog(textError);
            }
            return;
        }
    }
}
