﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyncVerticalToTFlexDocs
{
    public class AppAsconVertical
    {
        /// <summary>
        /// Содединение с базой данных Вертикаль
        /// </summary>
        SqlConnection conn { get; set; }
        SqlConnectionStringBuilder sqlBuilder;

        public List<string> listNameParamNull { get; set; }

        public string textError { get; set; }

        /// <summary>
        /// Класс для работы с базой данных Вертикаль
        /// </summary>
        public AppAsconVertical(string username, string password, string db, string source)
        {
            this.sqlBuilder = new SqlConnectionStringBuilder();
            this.sqlBuilder.UserID = username;
            this.sqlBuilder.Password = password;
            this.sqlBuilder.InitialCatalog = db;
            this.sqlBuilder.DataSource = source;
            this.conn = new SqlConnection(sqlBuilder.ConnectionString);
            this.textError = "";
        }

        private List<Dictionary<string, string>> getLlistRow(SqlDataReader reader)
        {
            List<Dictionary<string, string>> list = new List<Dictionary<string, string>>();
            int numCurrentRow = 0;
            while (reader.Read())
            {
                Dictionary<string, string> row = new Dictionary<string, string>();
                for (int i = 0; i < reader.FieldCount; i++)
                {
                    string name = reader.GetName(i);
                    string value;
                    try
                    {
                        value = reader.IsDBNull(i) ? "" : reader.GetValue(i).ToString();

                    }
                    catch (Exception ex)
                    {
                        value = "";
                        this.textError += String.Format("Ошибка при обработке строки {0} столбца {1}:\n{2}", numCurrentRow, i, ex.Message);
                    }
                    row.Add(name, value);
                }
                list.Add(row);
                numCurrentRow++;
            }




            return list;

        }






        /// <summary>
        /// Получить все данные о режущем инструменте из базы данных системы АСКОН
        /// </summary>
        public List<Dictionary<string, string>> getCT()
        {
            List<Dictionary<string, string>> listRow = new List<Dictionary<string, string>>();
            using (this.conn = new SqlConnection(sqlBuilder.ConnectionString))
            {
                this.conn.Open();
                StringBuilder builder = new StringBuilder();
                string cmd = "USE VRTSERVER_V75 ";
                cmd += "SELECT r1.NAMETIPRI as 'class_NAME', r1.GUID as 'class_GUID', r2.TIPRI as 'group_TIPRI', r2.GROUPRI as 'group_GROUPRI', r2.PEREXOD as 'group_PEREXOD', r2.GUID as 'group_GUID', r3.COMMENTS as 'group2_COMMENTS', r3.NAME_RI as 'group2_NAME_RI', r3.GOST as 'group2_GOST', r3.PRIM as 'group2_PRIM', r3.GUID as 'group2_GUID', r4.NAME as 'item_NAME', r4.OBOZN as 'item_OBOZN', r4.COD_START as 'item_COD_START', r4.MATRLOCATION as 'item_MATRLOCATION', r4.GUID as 'item_GUID', r4.FGUID as 'item_FGUID' ";
                cmd += "FROM RI4 r4 LEFT JOIN BO_NOAPPLY bo4 ON r4.GUID = bo4.GUID_BO, RI3 r3 LEFT JOIN BO_NOAPPLY bo3 ON r3.GUID = bo3.GUID_BO, RI2 r2 LEFT JOIN BO_NOAPPLY bo2 ON r2.GUID = bo2.GUID_BO, RI1 r1 LEFT JOIN BO_NOAPPLY bo1 ON r1.GUID = bo1.GUID_BO ";
                cmd += "WHERE r4.FGUID = r3.GUID AND r3.FGUID = r2.GUID AND r2.FGUID = r1.GUID and bo1.GUID_BO is null and bo2.GUID_BO is null and bo3.GUID_BO is null and bo4.GUID_BO is null ";
                cmd += "ORDER BY r1.GUID, r2.GUID, r3.GUID, r4.GUID;";

                builder.Append(cmd);
                using (SqlCommand sqlCommand = new SqlCommand(builder.ToString(), conn))
                {
                    using (SqlDataReader reader = sqlCommand.ExecuteReader())
                    {
                        listRow = this.getLlistRow(reader);
                    }
                }
                this.conn.Close();
            }
            return listRow;
        }


        /// <summary>
        /// Получить коллекцию динамических параметров/атрибутов для объекта из БД АСКОН
        /// </summary>
        /// <param name="guid">GUID объекта</param>
        /// <param name="fguid">GUID родителя объекта</param>
        /// <returns></returns>
        public List<Dictionary<string, string>> getDinamicParam(string guid, string fguid)
        {
            List<Dictionary<string, string>> listRow = new List<Dictionary<string, string>>();


            using (this.conn = new SqlConnection(sqlBuilder.ConnectionString))
            {
                this.conn.Open();
                StringBuilder builder = new StringBuilder();
                string cmd = String.Format("USE VRTSERVER_V75 ");
                cmd += String.Format("SELECT bo_ac.NAMESCREEN as 'NAMESCREEN', bo_ae.VALUEINT as 'VALUEINT', bo_ae.VALUEDOUBLE as 'VALUEDOUBLE', bo_ae.VALUESTR as 'VALUESTR', bo_ae.VALUETEXT as 'VALUETEXT', bo_ae.VALUETIMESTAMP as 'VALUETIMESTAMP' ");
                cmd += String.Format("FROM BO_ATTR_CLASSES bo_ac, BO_ATTR_LINKSGROUP bo_al, BO_ATTR_EXEMPLAR bo_ae ");
                cmd += String.Format("WHERE bo_al.IDTYPEATTR = bo_ac.ID AND bo_ae.IDLINK = bo_al.ID AND (bo_ae.VALUEINT is not NULL or bo_ae.VALUEDOUBLE is not NULL or bo_ae.VALUESTR is not NULL or bo_ae.VALUETEXT is not NULL) AND bo_ae.GUIDOBJECT like '{0}' and bo_al.NAMEGROUP like '{1}';", guid, fguid);
                builder.Append(cmd);
                using (SqlCommand sqlCommand = new SqlCommand(builder.ToString(), conn))
                {
                    using (SqlDataReader reader = sqlCommand.ExecuteReader())
                    {
                        listRow = this.getLlistRow(reader);
                    }
                }
                this.conn.Close();
            }
            return listRow;
        }


        /// <summary>
        /// Получить все данные об объектах "Вспомогательный инструмент" из базы данных системы АСКОН
        /// </summary>
        /// <returns></returns>
        public List<Dictionary<string, string>> getAT()
        {
            List<Dictionary<string, string>> listRow = new List<Dictionary<string, string>>();
            using (this.conn = new SqlConnection(sqlBuilder.ConnectionString))
            {
                this.conn.Open();
                StringBuilder strBuilder = new StringBuilder();
                string cmd = "USE VRTSERVER_V75 ";
                cmd += "SELECT v1.NAMETIPOPER as 'class_NAMETIPOPER', v1.GUID as 'class_GUID', v2.A as 'group_A', v2.B as 'group_B', v2.C as 'group_C', v2.GUID as 'group_GUID', v3.COMMENTS as 'group2_COMMENTS', v3.NAME_VI as 'group2_NAME_VI', v3.GOST as 'group2_GOST', v3.KODTIPVI as 'group2_KODTIPVI', v3.KODGRVI as 'group2_KODGRVI', v3.PRIMVI as 'group2_PRIMVI', v3.PICTUREBLOB as 'group2_PICTUREBLOB', v3.GUID as 'group2_GUID', v4.NAME as 'item_NAME', v4.OBOZN as 'item_OBOZN', v4.COD_START as 'item_COD_START', v4.GUID as 'item_GUID', v4.FGUID as 'item_FGUID'  ";
                cmd += "FROM VRTSERVER_V75.dbo.VI1 as v1 LEFT JOIN BO_NOAPPLY bo1 ON v1.GUID = bo1.GUID_BO, VRTSERVER_V75.dbo.VI2 as v2 LEFT JOIN BO_NOAPPLY bo2 ON v2.GUID = bo2.GUID_BO, VRTSERVER_V75.dbo.VI3 as v3 LEFT JOIN BO_NOAPPLY bo3 ON v3.GUID = bo3.GUID_BO, VRTSERVER_V75.dbo.VI4 as v4 LEFT JOIN BO_NOAPPLY bo4 ON v4.GUID = bo4.GUID_BO ";
                cmd += "WHERE v4.FGUID = v3.GUID AND v3.FGUID = v2.GUID AND v2.FGUID = v1.GUID and bo1.GUID_BO is null and bo2.GUID_BO is null and bo3.GUID_BO is null and bo4.GUID_BO is null ";
                cmd += "ORDER BY v1.GUID, v2.GUID, v3.GUID, v4.GUID;";
                strBuilder.Append(cmd);
                using (SqlCommand sqlCommand = new SqlCommand(strBuilder.ToString(), conn))
                {
                    using (SqlDataReader reader = sqlCommand.ExecuteReader())
                    {
                        listRow = this.getLlistRow(reader);
                    }
                }
                this.conn.Close();
            }
            return listRow;
        }

        /// <summary>
        /// Получить все данные об объектах "Режущая часть (пластинки)" из базы данных системы АСКОН
        /// </summary>
        /// <returns></returns>
        public List<Dictionary<string, string>> getCB()
        {
            List<Dictionary<string, string>> listRow = new List<Dictionary<string, string>>();
            using (this.conn = new SqlConnection(sqlBuilder.ConnectionString))
            {
                this.conn.Open();
                StringBuilder strBuilder = new StringBuilder();
                string cmd = "USE VRTSERVER_V75 ";
                cmd += "SELECT g1.NAME as 'class_NAME', g1.GUID as 'class_GUID', g2.NAME as 'group_NAME', g2.GUID as 'group_GUID', g3.COMMENTS as 'group2_COMMENTS', g3.FORMPLATE as 'group2_FORMPLATE', g3.NAME as 'group2_NAME', g3.OBOZN_ISO as 'group2_OBOZN_ISO', g3.GUID as 'group2_GUID', g4.COATING as 'item_COATING', g4.COD_START as 'item_COD_START', g4.D as 'item_D', g4.D1 as 'item_D1', g4.L as 'item_L', g4.MATRI as 'item_MATRI', g4.MATRLOCATION as 'item_MATRLOCATION', g4.OBOZN as 'item_OBOZN', g4.R as 'item_R', g4.S as 'item_S', g4.GUID as 'item_GUID', g4.FGUID as 'item_FGUID' ";
                cmd += "FROM RI_BLADE0 as g1 LEFT JOIN BO_NOAPPLY bo1 ON g1.GUID = bo1.GUID_BO, RI_BLADE1 as g2 LEFT JOIN BO_NOAPPLY bo2 ON g2.GUID = bo2.GUID_BO, RI_BLADE2 as g3 LEFT JOIN BO_NOAPPLY bo3 ON g3.GUID = bo3.GUID_BO, RI_BLADE3 as g4 LEFT JOIN BO_NOAPPLY bo4 ON g4.GUID = bo4.GUID_BO ";
                cmd += "WHERE g4.FGUID = g3.GUID AND g3.FGUID = g2.GUID AND g2.FGUID = g1.GUID and bo1.GUID_BO is null and bo2.GUID_BO is null and bo3.GUID_BO is null and bo4.GUID_BO is null ";
                cmd += "ORDER BY g1.GUID, g2.GUID, g3.GUID, g4.GUID;";

                strBuilder.Append(cmd);
                using (SqlCommand sqlCommand = new SqlCommand(strBuilder.ToString(), conn))
                {
                    using (SqlDataReader reader = sqlCommand.ExecuteReader())
                    {
                        listRow = this.getLlistRow(reader);
                    }
                }
                this.conn.Close();
            }
            return listRow;
        }



        /// <summary>
        /// Получить все данные об объектах "Измерительный инструмент" из базы данных системы АСКОН
        /// </summary>
        /// <returns></returns>
        public List<Dictionary<string, string>> getMI()
        {
            List<Dictionary<string, string>> listRow = new List<Dictionary<string, string>>();
            using (this.conn = new SqlConnection(sqlBuilder.ConnectionString))
            {
                this.conn.Open();
                StringBuilder strBuilder = new StringBuilder();
                string cmd = "USE VRTSERVER_V75 ";
                cmd += "SELECT g1.GROUPII as 'class_GROUPII', g1.GUID as 'class_GUID', g2.COMMENTS as 'group_COMMENTS', g2.NAME_II as 'group_NAME_II', g2.GOST as 'group_GOST', g2.KODTIPII as 'group_KODTIPII', g2.KODGRII as 'group_KODGRII', g2.PRIM as 'group_PRIM', g2.KODRAST as 'group_KODRAST', g2.GUID as 'group_GUID', g3.NAME as 'item_NAME', g3.OBOZN as 'item_OBOZN', g3.QUALITET as 'item_QUALITET', g3.COD_START as 'item_COD_START', g3.GUID as 'item_GUID', g3.FGUID as 'item_FGUID' ";
                cmd += "FROM II1 g1 LEFT JOIN BO_NOAPPLY bo1 ON g1.GUID = bo1.GUID_BO, II2 g2 LEFT JOIN BO_NOAPPLY bo2 ON g2.GUID = bo2.GUID_BO, II3 g3 LEFT JOIN BO_NOAPPLY bo3 ON g3.GUID = bo3.GUID_BO ";
                cmd += "WHERE g1.GUID = g2.FGUID AND g2.GUID = g3.FGUID and bo1.GUID_BO is null and bo2.GUID_BO is null and bo3.GUID_BO is null ";
                cmd += "ORDER BY g1.GUID, g2.GUID, g3.GUID;";
                strBuilder.Append(cmd);
                using (SqlCommand sqlCommand = new SqlCommand(strBuilder.ToString(), conn))
                {
                    using (SqlDataReader reader = sqlCommand.ExecuteReader())
                    {
                        listRow = this.getLlistRow(reader);
                    }
                }
                this.conn.Close();
            }
            return listRow;
        }



        /// <summary>
        /// Получить все данные о режущем инструменте из базы данных системы АСКОН
        /// </summary>
        public List<Dictionary<string, string>> getTara()
        {
            List<Dictionary<string, string>> listRow = new List<Dictionary<string, string>>();
            using (this.conn = new SqlConnection(sqlBuilder.ConnectionString))
            {
                this.conn.Open();
                StringBuilder builder = new StringBuilder();
                string cmd = "USE VRTSERVER_V75 ";
                cmd += "SELECT t1.GUID as 'class_GUID', t1.NAME as 'class_NAME', t2.B_TARA as 'item_B_TARA', t2.COD_START as 'item_COD_START', t2.GOST as 'item_GOST', t2.H_TARA as 'item_H_TARA', t2.NAME as 'item_NAME', t2.OBOZN as 'item_OBOZN', t2.PRIM as 'item_PRIM', t2.UN_TARA as 'item_UN_TARA', t2.GUID as 'item_GUID' ";
                cmd += "FROM TARA_NAME_TARA t1 LEFT JOIN BO_NOAPPLY bo1 ON t1.GUID = bo1.GUID_BO, TARA_GROUP t2 LEFT JOIN BO_NOAPPLY bo2 ON t2.GUID = bo2.GUID_BO ";
                cmd += "WHERE t1.GUID = t2.FGUID and bo1.GUID_BO is null and bo2.GUID_BO is null  ";
                cmd += "ORDER BY t1.GUID, t2.GUID;";

                builder.Append(cmd);
                using (SqlCommand sqlCommand = new SqlCommand(builder.ToString(), conn))
                {
                    using (SqlDataReader reader = sqlCommand.ExecuteReader())
                    {
                        listRow = this.getLlistRow(reader);
                    }
                }
                this.conn.Close();
            }
            return listRow;
        }



        /// <summary>
        /// Получить все данные об объектах "Измерительные приборы" из базы данных системы АСКОН
        /// </summary>
        /// <returns></returns>
        public List<Dictionary<string, string>> getMP()
        {
            List<Dictionary<string, string>> listRow = new List<Dictionary<string, string>>();
            using (this.conn = new SqlConnection(sqlBuilder.ConnectionString))
            {
                this.conn.Open();
                StringBuilder strBuilder = new StringBuilder();
                string cmd = "USE VRTSERVER_V75 ";
                cmd += "SELECT i1.A as 'class_A', i1.B 'class_B', i1.GUID 'class_GUID', i2.NAMETIPIZ 'group_NAMETIPIZ', i2.GUID 'group_GUID', i3.COD_START 'item_COD_START', i3.GOST 'item_GOST', i3.IZMSVR 'item_IZMSVR', i3.NAMEIZM 'item_NAMEIZM', i3.VALIZM 'item_VALIZM', i3.XARIZM 'item_XARIZM', i3.GUID 'item_GUID', i3.FGUID 'item_FGUID' ";
                cmd += "FROM IZ_SVR1 i1 LEFT JOIN BO_NOAPPLY bo1 ON i1.GUID = bo1.GUID_BO, IZ_SVR2 i2 LEFT JOIN BO_NOAPPLY bo2 ON i2.GUID = bo2.GUID_BO, IZ_SVR3 i3 LEFT JOIN BO_NOAPPLY bo3 ON i3.GUID = bo3.GUID_BO ";
                cmd += "WHERE i1.GUID = i2.FGUID AND i2.GUID = i3.FGUID and bo1.GUID_BO is null and bo2.GUID_BO is null and bo3.GUID_BO is null ";
                cmd += "ORDER BY i1.GUID, i2.GUID, i3.GUID;";
                strBuilder.Append(cmd);
                using (SqlCommand sqlCommand = new SqlCommand(strBuilder.ToString(), conn))
                {
                    using (SqlDataReader reader = sqlCommand.ExecuteReader())
                    {
                        listRow = this.getLlistRow(reader);
                    }
                }
                this.conn.Close();
            }
            return listRow;
        }




        /// <summary>
        /// Получить все данные об объектах "Штамповочная оснастка" из базы данных системы АСКОН
        /// </summary>
        /// <returns></returns>
        public List<Dictionary<string, string>> getSE()
        {
            List<Dictionary<string, string>> listRow = new List<Dictionary<string, string>>();
            using (this.conn = new SqlConnection(sqlBuilder.ConnectionString))
            {
                this.conn.Open();
                StringBuilder strBuilder = new StringBuilder();
                string cmd = "USE VRTSERVER_V75 ";
                cmd += "SELECT o1.A as 'class_A', o1.B as 'class_B', o1.GUID as 'class_GUID', o2.KODGR as 'group_KODGR', o2.KODTIP as 'group_KODTIP', o2.NAMEGROSN as 'group_NAMEGROSN', o2.GUID as 'group_GUID', o3.COD_START as 'item_COD_START', o3.KODGROSN as 'item_KODGROSN', o3.KODPODGROSN as 'item_KODPODGROSN', o3.NAMEOSN as 'item_NAMEOSN', o3.OSNAS as 'item_OSNAS', o3.GUID as 'item_GUID', o3.FGUID as 'item_FGUID'";
                cmd += "FROM OSN_SHT1 o1 LEFT JOIN BO_NOAPPLY bo1 ON o1.GUID = bo1.GUID_BO, OSN_SHT2 o2 LEFT JOIN BO_NOAPPLY bo2 ON o2.GUID = bo2.GUID_BO, OSN_SHT3 o3 LEFT JOIN BO_NOAPPLY bo3 ON o3.GUID = bo3.GUID_BO ";
                cmd += "WHERE o1.GUID = o2.FGUID AND o2.GUID = o3.FGUID AND bo1.GUID_BO is null AND bo2.GUID_BO is null AND bo3.GUID_BO is null ";
                cmd += "ORDER BY o1.GUID, o2.GUID, o3.GUID;";
                strBuilder.Append(cmd);
                using (SqlCommand sqlCommand = new SqlCommand(strBuilder.ToString(), conn))
                {
                    using (SqlDataReader reader = sqlCommand.ExecuteReader())
                    {
                        listRow = this.getLlistRow(reader);
                    }
                }
                this.conn.Close();
            }
            return listRow;
        }

        /// <summary>
        /// Получить все данные об объектах "Штамповочный инструмент" из базы данных системы АСКОН
        /// </summary>
        /// <returns></returns>
        public List<Dictionary<string, string>> getSHI()
        {
            List<Dictionary<string, string>> listRow = new List<Dictionary<string, string>>();
            using (this.conn = new SqlConnection(sqlBuilder.ConnectionString))
            {
                this.conn.Open();
                StringBuilder strBuilder = new StringBuilder();
                string cmd = "USE VRTSERVER_V75 ";
                cmd += "SELECT is1.A as 'class_A', is1.B as 'class_B', is1.GUID as 'class_GUID', is2.NAMEGRINS as 'group_NAMEGRINS', is2.KODTIPINS as 'group_KODTIPINS', is2.KODGRINS as 'group_KODGRINS', is2.GUID as 'group_GUID', is3.NAMEINSTR as 'item_NAMEINSTR', is3.INSRTSHT as 'item_INSRTSHT', is3.KODGR as 'item_KODGR', is3.KODPODGR as 'item_KODPODGR', is3.COD_START as 'item_COD_START', is3.GUID as 'item_GUID', is3.FGUID as 'item_FGUID' ";
                cmd += "FROM INS_SHT1 is1 LEFT JOIN BO_NOAPPLY bo1 ON is1.GUID = bo1.GUID_BO, INS_SHT2 is2 LEFT JOIN BO_NOAPPLY bo2 ON is2.GUID = bo2.GUID_BO, INS_SHT3 is3 LEFT JOIN BO_NOAPPLY bo3 ON is3.GUID = bo3.GUID_BO ";
                cmd += "WHERE is1.GUID = is2.FGUID AND is2.GUID = is3.FGUID AND bo1.GUID_BO is null AND bo2.GUID_BO is null AND bo3.GUID_BO is null ";
                cmd += "ORDER BY is1.GUID, is2.GUID, is3.GUID;";
                strBuilder.Append(cmd);
                using (SqlCommand sqlCommand = new SqlCommand(strBuilder.ToString(), conn))
                {
                    using (SqlDataReader reader = sqlCommand.ExecuteReader())
                    {
                        listRow = this.getLlistRow(reader);
                    }
                }
                this.conn.Close();
            }
            return listRow;
        }
        /// <summary>
        /// Получить все данные об объектах "Штамповочный инструмент" из базы данных системы АСКОН
        /// </summary>
        /// <returns></returns>
        public List<Dictionary<string, string>> getSP()
        {
            List<Dictionary<string, string>> listRow = new List<Dictionary<string, string>>();
            using (this.conn = new SqlConnection(sqlBuilder.ConnectionString))
            {
                this.conn.Open();
                StringBuilder strBuilder = new StringBuilder();
                string cmd = "USE VRTSERVER_V75 ";
                cmd += "SELECT p1.VID_PRISP as 'class_VID_PRISP', p1.GUID as 'class_GUID', p2.A as 'group_A', p2.GUID as 'group_GUID', p3.COMMENTS as 'group2_COMMENTS', p3.NAME_PRISP as 'group2_NAME_PRISP', p3.GOST as 'group2_GOST', p3.PRIM as 'group2_PRIM', p3.GUID as 'group2_GUID', p4.OBOZN as 'item_OBOZN', p4.COD_START as 'item_COD_START', p4.GUID as 'item_GUID', p4.FGUID as 'item_FGUID' ";
                cmd += "FROM PRISP1 p1 LEFT JOIN BO_NOAPPLY bo1 ON p1.GUID = bo1.GUID_BO, PRISP2 p2 LEFT JOIN BO_NOAPPLY bo2 ON p2.GUID = bo2.GUID_BO, PRISP3 p3 LEFT JOIN BO_NOAPPLY bo3 ON p3.GUID = bo3.GUID_BO, PRISP4 p4 LEFT JOIN BO_NOAPPLY bo4 ON p4.GUID = bo4.GUID_BO ";
                cmd += "WHERE p1.GUID = p2.FGUID AND p2.GUID = p3.FGUID AND p3.GUID = p4.FGUID AND bo1.GUID_BO is null AND bo2.GUID_BO is null AND bo3.GUID_BO is null AND bo4.GUID_BO is NULL ";
                cmd += "ORDER BY p1.GUID, p2.GUID, p3.GUID, p4.GUID;";
                strBuilder.Append(cmd);
                using (SqlCommand sqlCommand = new SqlCommand(strBuilder.ToString(), conn))
                {
                    using (SqlDataReader reader = sqlCommand.ExecuteReader())
                    {
                        listRow = this.getLlistRow(reader);
                    }
                }
                this.conn.Close();
            }
            return listRow;
        }


        /// <summary>
        /// Получить все данные об объектах "Штамповочный инструмент" из базы данных системы АСКОН
        /// </summary>
        /// <returns></returns>
        public List<Dictionary<string, string>> getRL()
        {
            List<Dictionary<string, string>> listRow = new List<Dictionary<string, string>>();
            using (this.conn = new SqlConnection(sqlBuilder.ConnectionString))
            {
                this.conn.Open();
                StringBuilder strBuilder = new StringBuilder();
                string cmd = "USE VRTSERVER_V75 ";
                cmd += "SELECT i1.VID as 'class_VID', i1.GUID as 'class_GUID', i2.NAMEINSR as 'item_NAMEINSR', i2.OBOZINST as 'item_OBOZINST', i2.COMMENTS as 'item_COMMENTS', i2.COD_START as 'item_COD_START', i2.GUID as 'item_GUID', i2.FGUID as 'item_FGUID' ";
                cmd += "FROM INST_POK1 i1 LEFT JOIN BO_NOAPPLY bo1 ON i1.GUID = bo1.GUID_BO, INST_POK2 i2 LEFT JOIN BO_NOAPPLY bo2 ON i2.GUID = bo2.GUID_BO ";
                cmd += "WHERE i1.GUID = i2.FGUID AND bo1.GUID_BO is null AND bo2.GUID_BO is null ";
                cmd += "ORDER BY i1.GUID, i2.GUID;";
                strBuilder.Append(cmd);
                using (SqlCommand sqlCommand = new SqlCommand(strBuilder.ToString(), conn))
                {
                    using (SqlDataReader reader = sqlCommand.ExecuteReader())
                    {
                        listRow = this.getLlistRow(reader);
                    }
                }
                this.conn.Close();
            }
            return listRow;
        }

        /// <summary>
        /// Получить все данные об объектах "Штамповочный инструмент" из базы данных системы АСКОН
        /// </summary>
        /// <returns></returns>
        public List<Dictionary<string, string>> getRT()
        {
            List<Dictionary<string, string>> listRow = new List<Dictionary<string, string>>();
            using (this.conn = new SqlConnection(sqlBuilder.ConnectionString))
            {
                this.conn.Open();
                StringBuilder strBuilder = new StringBuilder();
                string cmd = "USE VRTSERVER_V75 ";
                cmd += "SELECT o1.NAMEVID as 'class_NAMEVID', o1.GUID as 'class_GUID', o2.NAMETIPOSNTRM as 'group_NAMETIPOSNTRM', o2.GUID as 'group_GUID', o3.NAMEOSN as 'item_NAMEOSN', o3.OSNAS as 'item_OSNAS', o3.COD_START as 'item_COD_START', o3.GUID as 'item_GUID', o3.FGUID as 'item_FGUID' ";
                cmd += "FROM OSN_TR1 o1 LEFT JOIN BO_NOAPPLY bo1 ON o1.GUID = bo1.GUID_BO, OSN_TR2 o2 LEFT JOIN BO_NOAPPLY bo2 ON o2.GUID = bo2.GUID_BO, OSN_TR3 o3 LEFT JOIN BO_NOAPPLY bo3 ON o3.GUID = bo3.GUID_BO ";
                cmd += "WHERE o1.GUID = o2.FGUID AND o2.GUID = o3.FGUID AND bo1.GUID_BO is null AND bo2.GUID_BO is null AND bo3.GUID_BO is null ";
                cmd += "ORDER BY o1.GUID, o2.GUID, o3.GUID;";
                strBuilder.Append(cmd);
                using (SqlCommand sqlCommand = new SqlCommand(strBuilder.ToString(), conn))
                {
                    using (SqlDataReader reader = sqlCommand.ExecuteReader())
                    {
                        listRow = this.getLlistRow(reader);
                    }
                }
                this.conn.Close();
            }
            return listRow;
        }



        /// <summary>
        /// Получить все данные об объектах "Приборы" из базы данных системы АСКОН
        /// </summary>
        /// <returns></returns>
        public List<Dictionary<string, string>> getP()
        {
            List<Dictionary<string, string>> listRow = new List<Dictionary<string, string>>();
            using (this.conn = new SqlConnection(sqlBuilder.ConnectionString))
            {
                this.conn.Open();
                StringBuilder strBuilder = new StringBuilder();
                string cmd = "USE VRTSERVER_V75 ";
                cmd += "SELECT p1.GUID as 'class_GUID', p1.NAME as 'class_NAME', p2.NAME as 'item_NAME', p2.COD as 'item_COD', p2.OBOZN as 'item_OBOZN', p2.GUID as 'item_GUID', p2.FGUID as 'item_FGUID' ";
                cmd += "FROM PRB_VID p1 LEFT JOIN BO_NOAPPLY bo1 ON p1.GUID = bo1.GUID_BO, PRB_MARKA p2 LEFT JOIN BO_NOAPPLY bo2 ON p2.GUID = bo2.GUID_BO ";
                cmd += "WHERE p1.GUID = p2.FGUID AND bo1.GUID_BO is null AND bo2.GUID_BO is null ";
                cmd += "ORDER By p1.GUID, p2.GUID;";
                strBuilder.Append(cmd);
                using (SqlCommand sqlCommand = new SqlCommand(strBuilder.ToString(), conn))
                {
                    using (SqlDataReader reader = sqlCommand.ExecuteReader())
                    {
                        listRow = this.getLlistRow(reader);
                    }
                }
                this.conn.Close();
            }
            return listRow;
        }




        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<Dictionary<string, string>> getSBO()
        {
            List<Dictionary<string, string>> listRow = new List<Dictionary<string, string>>();
            using (this.conn = new SqlConnection(sqlBuilder.ConnectionString))
            {
                this.conn.Open();
                StringBuilder strBuilder = new StringBuilder();
                string cmd = "USE VRTSERVER_V75 ";
                cmd += "SELECT o1.A as 'class_A', o1.B as 'class_B', o1.GUID as 'class_GUID', o2.OSNASGR as 'group_OSNASGR', o2.KODTIPOSN as 'group_KODTIPOSN', o2.KODGROSN as 'group_KODGROSN ', o2.GUID as 'group_GUID', o3.NAMEOSN as 'item_NAMEOSN', o3.OSNSBR as 'item_OSNSBR', o3.XAROSN as 'item_XAROSN', o3.VALOSN as 'item_VALOSN', o3.KODGR as 'item_KODGR', o3.KODPODGR as 'item_KODPODGR', o3.COD_START as 'item_COD_START', o3.GUID as 'item_GUID', o3.FGUID as 'item_FGUID' ";
                cmd += "FROM OSN_SB1 o1 LEFT JOIN BO_NOAPPLY bo1 ON o1.GUID = bo1.GUID_BO, OSN_SB2 o2 LEFT JOIN BO_NOAPPLY bo2 ON o2.GUID = bo2.GUID_BO, OSN_SB3 o3 LEFT JOIN BO_NOAPPLY bo3 ON o3.GUID = bo3.GUID_BO ";
                cmd += "WHERE o1.GUID = o2.FGUID AND o2.GUID = o3.FGUID AND bo1.GUID_BO is null AND bo2.GUID_BO is null AND bo3.GUID_BO is null ";
                cmd += "ORDER By o1.GUID, o2.GUID, o3.GUID;";
                strBuilder.Append(cmd);
                using (SqlCommand sqlCommand = new SqlCommand(strBuilder.ToString(), conn))
                {
                    using (SqlDataReader reader = sqlCommand.ExecuteReader())
                    {
                        listRow = this.getLlistRow(reader);
                    }
                }
                this.conn.Close();
            }
            return listRow;
        }




        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<Dictionary<string, string>> getSVO()
        {
            List<Dictionary<string, string>> listRow = new List<Dictionary<string, string>>();
            using (this.conn = new SqlConnection(sqlBuilder.ConnectionString))
            {
                this.conn.Open();
                StringBuilder strBuilder = new StringBuilder();
                string cmd = "USE VRTSERVER_V75 ";
                cmd += "SELECT o1.A as 'class_A', o1.B as 'class_B', o1.GUID as 'class_GUID', o2.NAMEOSN as 'item_NAMEOSN', o2.OSNSVR as 'item_OSNSVR', o2.XAROSN as 'item_XAROSN', o2.VALOSN as 'item_VALOSN', o2.KODTIPOSN as 'item_KODTIPOSN', o2.KODGROSN as 'item_KODGROSN', o2.COD_START as 'item_COD_START', o2.GUID as 'item_GUID', o2.FGUID as 'item_FGUID' ";
                cmd += "FROM OSN_SVR1 o1 LEFT JOIN BO_NOAPPLY bo1 ON o1.GUID = bo1.GUID_BO, OSN_SVR2 o2 LEFT JOIN BO_NOAPPLY bo2 ON o2.GUID = bo2.GUID_BO ";
                cmd += "WHERE o1.GUID = o2.FGUID AND bo1.GUID_BO is null AND bo2.GUID_BO is null ";
                cmd += "ORDER BY o1.GUID, o2.GUID;";
                strBuilder.Append(cmd);
                using (SqlCommand sqlCommand = new SqlCommand(strBuilder.ToString(), conn))
                {
                    using (SqlDataReader reader = sqlCommand.ExecuteReader())
                    {
                        listRow = this.getLlistRow(reader);
                    }
                }
                this.conn.Close();
            }
            return listRow;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<Dictionary<string, string>> getSI()
        {
            List<Dictionary<string, string>> listRow = new List<Dictionary<string, string>>();
            using (this.conn = new SqlConnection(sqlBuilder.ConnectionString))
            {
                this.conn.Open();
                StringBuilder strBuilder = new StringBuilder();
                string cmd = "USE VRTSERVER_V75 ";
                cmd += "SELECT s1.A as 'class_A', s1.B as 'class_B', s1.GUID as 'class_GUID', s2.NAMEGRINS as 'group_NAMEGRINS', s2.KODTIPINS as 'group_KODTIPINS', s2.KODGRINS as 'group_KODGRINS', s2.GUID as 'group_GUID', s3.NAMEINSTR as 'group2_NAMEINSTR', s3.INSRTSHT as 'group2_INSRTSHT', s3.GOST as 'group2_GOST', s3.KODGR as 'group2_KODGR', s3.KODPODGR as 'group2_KODPODGR', s3.PRIM as 'group2_PRIM', s3.GUID as 'group2_GUID', s4.OBOZN as 'item_OBOZN', s4.COD_START as 'item_COD_START', s4.GUID as 'item_GUID', s4.FGUID as 'item_FGUID' ";
                cmd += "FROM SLI1 s1 LEFT JOIN BO_NOAPPLY bo1 ON s1.GUID = bo1.GUID_BO, SLI2 s2 LEFT JOIN BO_NOAPPLY bo2 ON s2.GUID = bo2.GUID_BO, SLI3 s3 LEFT JOIN BO_NOAPPLY bo3 ON s3.GUID = bo3.GUID_BO, SLI4 s4 LEFT JOIN BO_NOAPPLY bo4 ON s4.GUID = bo4.GUID_BO ";
                cmd += "WHERE s1.GUID = s2.FGUID AND s2.GUID = s3.FGUID AND s3.GUID = s4.FGUID ";
                cmd += "ORDER BY s1.GUID, s2.GUID, s3.GUID, s4.GUID;";
                strBuilder.Append(cmd);
                using (SqlCommand sqlCommand = new SqlCommand(strBuilder.ToString(), conn))
                {
                    using (SqlDataReader reader = sqlCommand.ExecuteReader())
                    {
                        listRow = this.getLlistRow(reader);
                    }
                }
                this.conn.Close();
            }
            return listRow;
        }
    }

}