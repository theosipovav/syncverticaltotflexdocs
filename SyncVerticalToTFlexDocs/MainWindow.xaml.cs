﻿using SyncVerticalToTFlexDocs.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TFlex.DOCs.Model.References;

namespace SyncVerticalToTFlexDocs
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //
        private AppAsconVertical appAsconVertical;
        private AppTflex appTflex;
        private AppData appData;
        //
        private Thread threadRun;
        private delegate void SafeCallDelegate(int pre, int post);
        private delegate void SafeCallDelegateLog(string str);
        //
        public MainWindow()
        {
            InitializeComponent();
            ButtonRunSync.IsEnabled = false;
            ButtonRunCancel.IsEnabled = false;
            appData = new AppData();
            updateLog("Ожидание подключение.");
        }

        /// <summary>
        /// Запстить импорт
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonRunSync_Click(object sender, RoutedEventArgs e)
        {
            if (CheckBoxCT.IsChecked == true)
            {
                threadRun = new Thread(new ThreadStart(importCT));
                threadRun.Start();
                ButtonRunSync.IsEnabled = false;
                ButtonRunCancel.IsEnabled = true;
                return;
            }
            if (CheckBoxAT.IsChecked == true)
            {
                threadRun = new Thread(new ThreadStart(importAT));
                threadRun.Start();
                ButtonRunSync.IsEnabled = false;
                ButtonRunCancel.IsEnabled = true;
                return;
            }
            if (CheckBoxCB.IsChecked == true)
            {
                threadRun = new Thread(new ThreadStart(importCB));
                threadRun.Start();
                ButtonRunSync.IsEnabled = false;
                ButtonRunCancel.IsEnabled = true;
                return;
            }
            if (CheckBoxMI.IsChecked == true)
            {
                threadRun = new Thread(new ThreadStart(importMI));
                threadRun.Start();
                ButtonRunSync.IsEnabled = false;
                ButtonRunCancel.IsEnabled = true;
                return;
            }
            if (CheckBoxTara.IsChecked == true)
            {
                threadRun = new Thread(new ThreadStart(importTara));
                threadRun.Start();
                ButtonRunSync.IsEnabled = false;
                ButtonRunCancel.IsEnabled = true;
                return;
            }
            if (CheckBoxMP.IsChecked == true)
            {
                threadRun = new Thread(new ThreadStart(importMP));
                threadRun.Start();
                ButtonRunSync.IsEnabled = false;
                ButtonRunCancel.IsEnabled = true;
                return;
            }
            if (CheckBoxSE.IsChecked == true)
            {
                threadRun = new Thread(new ThreadStart(importSE));
                threadRun.Start();
                ButtonRunSync.IsEnabled = false;
                ButtonRunCancel.IsEnabled = true;
                return;
            }
            if (CheckBoxSHI.IsChecked == true)
            {
                threadRun = new Thread(new ThreadStart(importSI));
                threadRun.Start();
                ButtonRunSync.IsEnabled = false;
                ButtonRunCancel.IsEnabled = true;
                return;
            }
            if (CheckBoxSP.IsChecked == true)
            {
                threadRun = new Thread(new ThreadStart(importSP));
                threadRun.Start();
                ButtonRunSync.IsEnabled = false;
                ButtonRunCancel.IsEnabled = true;
                return;
            }
            if (CheckBoxRL.IsChecked == true)
            {
                threadRun = new Thread(new ThreadStart(importRL));
                threadRun.Start();
                ButtonRunSync.IsEnabled = false;
                ButtonRunCancel.IsEnabled = true;
                return;
            }
            if (CheckBoxRT.IsChecked == true)
            {
                threadRun = new Thread(new ThreadStart(importRT));
                threadRun.Start();
                ButtonRunSync.IsEnabled = false;
                ButtonRunCancel.IsEnabled = true;
                return;
            }
            if (CheckBoxP.IsChecked == true)
            {
                threadRun = new Thread(new ThreadStart(importP));
                threadRun.Start();
                ButtonRunSync.IsEnabled = false;
                ButtonRunCancel.IsEnabled = true;
                return;
            }
            if (CheckBoxSBO.IsChecked == true)
            {
                threadRun = new Thread(new ThreadStart(importSBO));
                threadRun.Start();
                ButtonRunSync.IsEnabled = false;
                ButtonRunCancel.IsEnabled = true;
                return;
            }
            if (CheckBoxSVO.IsChecked == true)
            {
                threadRun = new Thread(new ThreadStart(importSVO));
                threadRun.Start();
                ButtonRunSync.IsEnabled = false;
                ButtonRunCancel.IsEnabled = true;
                return;
            }
            if (CheckBoxSI.IsChecked == true)
            {
                threadRun = new Thread(new ThreadStart(importSI));
                threadRun.Start();
                ButtonRunSync.IsEnabled = false;
                ButtonRunCancel.IsEnabled = true;
                return;
            }
        }

        private void ButtonConnect_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                updateLog("Подключение к БД Вертикаль...");
                this.appAsconVertical = new AppAsconVertical("viewer", "Vwr18Ui", "VRTSERVER_V75", "ascondbms\\start");
                updateLog("Подключение к БД Вертикаль - выполнено.");
                updateLog("Подключение к серверу T-Flex DOCs...");
                this.appTflex = new AppTflex("oav", "Ty12Vr75", "tflexas:21921");
                updateLog("Подключение к серверу T-Flex DOCs - выполнено");
                ButtonRunSync.IsEnabled = true;
                ButtonConfig.IsEnabled = false;
                ButtonConnect.IsEnabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// Добавить текст в запись логов
        /// </summary>
        /// <param name="text"></param>
        private void updateLog(string text)
        {
            this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                TextBlockLogs.Text += String.Format("{0}\r\n", text);
            });
        }

        /// <summary>
        /// Обновить прогресбар
        /// </summary>
        /// <param name="valForProgressBarLoad">Текущее значение</param>
        /// <param name="maxForProgressBarLoad">максимальное значение</param>
        private void updateProgresBarPre(int valForProgressBarLoad, int maxForProgressBarLoad, int valueForProgressBarLoadAll, int endForProgressBarLoadAll)
        {
            this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                ProgressBarLoad.Value = valForProgressBarLoad;
                ProgressBarLoad.Maximum = maxForProgressBarLoad;
                ProgressBarLoadAll.Value = valueForProgressBarLoadAll;
                ProgressBarLoadAll.Maximum = endForProgressBarLoadAll;
            });
        }

        private List<string> getListParamNameIsNull(List<DinamicParam> listDinamicParams)
        {

            List<string> listParamNameIsNull = new List<string>();
            foreach (DinamicParam dinamicParam in listDinamicParams)
            {
                foreach (KeyValuePair<string, string> p in dinamicParam.parameters)
                {
                    if (p.Value != "" && listParamNameIsNull.Count(x => x == p.Key) == 0)
                    {
                        listParamNameIsNull.Add(p.Key);
                    }
                }
            }
            string text = "";
            foreach (string s in listParamNameIsNull)
            {
                text += String.Format("{0}\n", s);
            }

            string path = System.IO.Path.GetTempPath() + @"/" + System.IO.Path.GetRandomFileName() + ".txt";

            using (Stream streamFile = File.Create(path))
            {
                using (StreamWriter streamWriter = new StreamWriter(streamFile))
                {
                    streamWriter.Write(text);
                    streamWriter.Close();
                }
                streamFile.Close();
            }

            Process.Start(new ProcessStartInfo("explorer.exe", " /select, " + path));



            return listParamNameIsNull;

        }

        private Dictionary<string, string> getParametersForGroup(Dictionary<string, string> collection, string tagGroup, string shortname, string nameDefault, Dictionary<string, string> addParameters)
        {
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            foreach (KeyValuePair<string, string> p in collection)
            {
                if (p.Key.Contains(tagGroup))
                {
                    string name = appData.replaceNameSQLParam(p.Key, shortname);
                    string value = AppData.replaceChars(p.Value);
                    if (parameters.Count(x => x.Key == name) == 0)
                    {
                        parameters.Add(name, value);
                    }
                    else
                    {
                        if (parameters[name] == "")
                        {
                            parameters[name] = value;
                        }
                    }

                }
            }
            if (nameDefault != "" && nameDefault != null)
            {
                if (!parameters.ContainsKey("Наименование"))
                {
                    parameters.Add("Наименование", nameDefault);
                }
            }
            if (addParameters != null)
            {
                foreach (KeyValuePair<string, string> parameter in addParameters)
                {
                    if (!parameters.ContainsKey(parameter.Key))
                    {
                        parameters.Add(parameter.Key, parameter.Value);
                    }
                }

            }
            return parameters;
        }


        /// <summary>
        /// Генерация коллекции динамических параметров для загруженных объектов
        /// </summary>
        /// <param name="listLoadObjects"></param>
        /// <returns></returns>
        private List<DinamicParam> genListDinamicParams(List<Dictionary<string, string>> listLoadObjects)
        {
            updateLog("Загрузка динамических параметров для объектов режущих инструментов из базы данных Вертикаль.");
            List<DinamicParam> listDinamicParams = new List<DinamicParam>();
            updateProgresBarPre(0, listLoadObjects.Count, 0, listLoadObjects.Count);
            for (int i = 0; i < listLoadObjects.Count; i++)
            {
                string guid = listLoadObjects[i]["item_GUID"];
                string fguid = listLoadObjects[i]["item_FGUID"];
                List<Dictionary<string, string>> p = appAsconVertical.getDinamicParam(guid, fguid);
                if (p.Count > 0)
                {
                    DinamicParam dinamicParam = new DinamicParam(guid, fguid, p);
                    listDinamicParams.Add(dinamicParam);
                }
                updateProgresBarPre(i, listLoadObjects.Count, 0, listLoadObjects.Count);

            }
            return listDinamicParams;
        }




        /// <summary>
        /// Импорт объектов "Тара" из базы данных системы АСКОН
        /// </summary>
        public void importTara()
        {
            string shortname = "Тара";
            string textError = "";
            string guidClass;
            int countItemsAll = 0;
            updateLog("Загрузка режущих инструментов из базы данных Вертикаль.");
            List<Dictionary<string, string>> listLoadObjects = appAsconVertical.getTara();
            updateLog("Загружено " + listLoadObjects.Count + " объектов.");
            List<DinamicParam> listDinamicParams = genListDinamicParams(listLoadObjects);
            updateLog("Синхронизация объектов T-Flex DOCs.");
            string nameGroupClass = shortname;
            foreach (ICollection<Dictionary<string, string>> group in listLoadObjects.GroupBy(x => x["class_GUID"]))
            {
                string nameGroup = group.First()["class_NAME"].Trim(' ');
                string guidGroup = group.First()["class_GUID"];
                string tagGroup = "class_";
                try
                {
                    guidClass = appData.getGuidTool(nameGroupClass, shortname, "Группа");
                    Dictionary<string, string> parametersGroup = getParametersForGroup(group.First(), tagGroup, shortname, nameGroup, null);
                    ReferenceObject folder = appTflex.createObjectCuttingTool(guidClass, parametersGroup);
                    updateLog(String.Format("Синхронизация {0} > {1} > {2} объектов.", nameGroupClass, nameGroup, group.Count()));
                    int countItems = 0;
                    foreach (Dictionary<string, string> groupItems in group)
                    {
                        string nameGroupItem = groupItems["item_NAME"].Trim(' ');
                        string guidGroupItem = groupItems["item_GUID"];
                        try
                        {
                            guidClass = appData.getGuidTool(nameGroupClass, shortname, "Экземпляр");
                            Dictionary<string, string> paramsItems = getParametersForGroup(groupItems, "item_", shortname, nameGroupItem, parametersGroup);
                            foreach (DinamicParam param in listDinamicParams.Where(x => x.guid == groupItems["item_GUID"] && x.fguid == groupItems["class_GUID"]))
                            {
                                foreach (KeyValuePair<string, string> parameterItem in param.parameters)
                                {
                                    string name = parameterItem.Key;
                                    string value = AppData.replaceChars(parameterItem.Value).Trim(' ');
                                    paramsItems.Add(name, value);
                                }
                            }
                            ReferenceObject obj = appTflex.createObjectCuttingTool(guidClass, paramsItems, folder);
                            updateProgresBarPre(countItems++, listLoadObjects.Count, countItemsAll++, listLoadObjects.Count);
                        }
                        catch (Exception ex)
                        {
                            System.Diagnostics.Debugger.Break();
                            textError += String.Format("{0} > {1} > {2}", nameGroupClass, nameGroup, nameGroupItem);
                            textError += String.Format("\n{0}", ex.Message);
                        }
                    }


                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debugger.Break();
                    textError += String.Format("{0} > {1}", nameGroupClass, nameGroup);
                    textError += String.Format("\n{0}", ex.Message);
                }
            }
        }


        /// <summary>
        /// Импорт объектов для справочника "Вспомогательный инструмент" из базы данных системы АСКОН
        /// </summary>
        public void importAT()
        {
            string shortname = "ВИ";
            string textError = "";
            int countAllItems = 0;
            updateLog(String.Format("Загрузка объектов <{0}> из базы данных Вертикаль.", shortname));
            List<Dictionary<string, string>> listLoadObjects = appAsconVertical.getAT();
            updateLog("Загружено " + listLoadObjects.Count + " объектов.");

            List<DinamicParam> listDinamicParams = genListDinamicParams(listLoadObjects);

            updateLog("Синхронизация объектов T-Flex DOCs.");

            // Создания класса для гурпп
            foreach (var groupClass in listLoadObjects.GroupBy(x => x["class_GUID"]))
            {
                string nameGroupClass = appData.replaceNameParameterVertical(groupClass.First()["class_NAMETIPOPER"].Trim(' '), shortname);
                string guidGroupClass = groupClass.First()["class_GUID"];
                string guidClass = appData.getGuidTool(shortname, shortname, "Группа");
                string tagGroup = "class_";
                try
                {
                    Dictionary<string, string> parametersGroupClass = getParametersForGroup(groupClass.First(), tagGroup, shortname, nameGroupClass, null);
                    ReferenceObject folderClass = appTflex.createObjectCuttingTool(guidClass, parametersGroupClass);
                    // Создание группы 1 уровня
                    foreach (var group in groupClass.GroupBy(x => x["group_GUID"]))
                    {
                        string nameGroup = group.First()["group_A"].Trim(' ');
                        string guidGroup = group.First()["group_GUID"];
                        guidClass = appData.getGuidTool(shortname, shortname, "Группа");
                        tagGroup = "group_";
                        try
                        {
                            Dictionary<string, string> parametersGroup = getParametersForGroup(group.First(), tagGroup, shortname, nameGroup, parametersGroupClass);
                            ReferenceObject folder = appTflex.createObjectCuttingTool(guidClass, parametersGroup, folderClass);
                            // Создание группы 2 уровня
                            foreach (var group2 in group.GroupBy(x => x["group2_GUID"]))
                            {
                                string nameGroup2 = String.Format("{0} {1}", group2.First()["group2_NAME_VI"], group2.First()["group2_GOST"]).Trim(' ');
                                string guidGroup2 = group2.First()["group2_GUID"];
                                guidClass = appData.getGuidTool(shortname, shortname, "Группа");
                                tagGroup = "group2_";
                                try
                                {
                                    Dictionary<string, string> parametersGroup2 = getParametersForGroup(group2.First(), tagGroup, shortname, nameGroup2, parametersGroup);
                                    ReferenceObject folder2 = appTflex.createObjectCuttingTool(guidClass, parametersGroup2, folder);
                                    // Создание экземпляра
                                    updateLog(String.Format("Синхронизация {0} > {1} > {2}: {3} объектов.", nameGroupClass, nameGroup, nameGroup2, group2.Count()));
                                    int countItems = 0;
                                    foreach (Dictionary<string, string> groupItems in group2)
                                    {
                                        string nameGroupItem = String.Format("{0} {1}", groupItems["item_NAME"], groupItems["item_OBOZN"]).Trim(' '); ;
                                        string guidGroupItem = groupItems["item_GUID"];

                                        guidClass = appData.getGuidTool(shortname, shortname, "Экземпляр");
                                        tagGroup = "item_";
                                        try
                                        {
                                            Dictionary<string, string> paramsItems = getParametersForGroup(groupItems, tagGroup, shortname, nameGroupItem, parametersGroup2);
                                            foreach (DinamicParam param in listDinamicParams.Where(x => x.guid == groupItems["item_GUID"] && x.fguid == groupItems["item_FGUID"]))
                                            {
                                                foreach (KeyValuePair<string, string> parameterItem in param.parameters)
                                                {
                                                    string name = parameterItem.Key;
                                                    string value = AppData.replaceChars(parameterItem.Value).Trim(' ');
                                                    paramsItems.Add(name, value);

                                                }
                                            }
                                            ReferenceObject obj = appTflex.createObjectCuttingTool(guidClass, paramsItems, folder2);
                                            countItems++;
                                            updateProgresBarPre(countItems, group2.Count(), 0, listLoadObjects.Count);
                                        }
                                        catch (Exception ex)
                                        {
                                            System.Diagnostics.Debugger.Break();
                                            textError += String.Format("{0} > {1} > {2} > {3}", nameGroupClass, nameGroup, nameGroup2, nameGroupItem);
                                            textError += String.Format("\n{0}", ex.Message);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    System.Diagnostics.Debugger.Break();
                                    textError += String.Format("{0} > {1} > {2}", nameGroupClass, nameGroup, nameGroup2);
                                    textError += String.Format("\n{0}", ex.Message);
                                }

                            }
                        }
                        catch (Exception ex)
                        {
                            System.Diagnostics.Debugger.Break();
                            textError += String.Format("{0} > {1}", nameGroupClass, nameGroup);
                            textError += String.Format("\n{0}", ex.Message);
                        }

                    }
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debugger.Break();
                    textError += String.Format("{0}", nameGroupClass);
                    textError += String.Format("\n{0}", ex.Message);
                }

            }
            return;
        }


        /// <summary>
        /// Импорт объектов для справочника "Измерительные приборы" из базы данных системы АСКОН
        /// </summary>
        public void importMP()
        {
            string shortname = "ИП";
            string textError = "";
            int countAllItems = 0;
            updateLog(String.Format("Загрузка объектов <{0}> из базы данных Вертикаль.", shortname));
            List<Dictionary<string, string>> listLoadObjects = appAsconVertical.getMP();
            updateLog("Загружено " + listLoadObjects.Count + " объектов.");

            List<DinamicParam> listDinamicParams = genListDinamicParams(listLoadObjects);
            updateLog("Синхронизация объектов T-Flex DOCs.");

            // Создания класса для гурпп
            foreach (ICollection<Dictionary<string, string>> groupClass in listLoadObjects.GroupBy(x => x["class_GUID"]))
            {
                string nameGroupClass = String.Format("{0} {1}", groupClass.First()["class_A"], groupClass.First()["class_B"]).Trim(' ');
                string guidGroupClass = groupClass.First()["class_GUID"];
                string guidClass = appData.getGuidTool(shortname, shortname, "Группа");
                string tagGroup = "class_";
                try
                {
                    Dictionary<string, string> parametersGroupClass = getParametersForGroup(groupClass.First(), tagGroup, shortname, nameGroupClass, null);
                    ReferenceObject folderClass = appTflex.createObjectCuttingTool(guidClass, parametersGroupClass);
                    foreach (ICollection<Dictionary<string, string>> group in groupClass.GroupBy(x => x["group_GUID"]))
                    {
                        string nameGroup = group.First()["group_NAMETIPIZ"].Trim(' ');
                        string guidGroup = group.First()["group_GUID"];
                        guidClass = appData.getGuidTool(shortname, shortname, "Группа");
                        tagGroup = "group_";
                        try
                        {
                            Dictionary<string, string> parametersGroup = getParametersForGroup(group.First(), tagGroup, shortname, nameGroup, parametersGroupClass);
                            foreach (KeyValuePair<string, string> parameter in parametersGroupClass)
                            {
                                if (!parametersGroup.ContainsKey(parameter.Key))
                                {
                                    parametersGroup.Add(parameter.Key, parameter.Value);
                                }
                            }
                            ReferenceObject folder = appTflex.createObjectCuttingTool(guidClass, parametersGroup, folderClass);
                            //
                            // Создание экземпляра
                            updateLog(String.Format("Синхронизация {0} > {1} > {2} объектов.", nameGroupClass, nameGroup, group.Count()));
                            int countItems = 0;
                            foreach (Dictionary<string, string> groupItems in group)
                            {
                                string nameGroupItem = groupItems["item_NAMEIZM"];
                                string guidGroupItem = groupItems["item_GUID"];
                                guidClass = appData.getGuidTool(shortname, shortname, "Экземпляр");
                                tagGroup = "item_";
                                try
                                {
                                    Dictionary<string, string> paramsItems = getParametersForGroup(groupItems, tagGroup, shortname, nameGroupItem, parametersGroup);
                                    foreach (DinamicParam param in listDinamicParams.Where(x => x.guid == groupItems["item_GUID"] && x.fguid == groupItems["item_FGUID"]))
                                    {
                                        foreach (KeyValuePair<string, string> parameterItem in param.parameters)
                                        {
                                            string name = parameterItem.Key;
                                            string value = AppData.replaceChars(parameterItem.Value).Trim(' ');
                                            paramsItems.Add(name, value);
                                        }
                                    }
                                    ReferenceObject obj = appTflex.createObjectCuttingTool(guidClass, paramsItems, folder);
                                    countItems++;
                                    updateProgresBarPre(countItems, group.Count(), countAllItems++, listLoadObjects.Count);
                                }
                                catch (Exception ex)
                                {
                                    System.Diagnostics.Debugger.Break();
                                    textError += String.Format("{0} > {1} > {2}", nameGroupClass, nameGroup, nameGroupItem);
                                    textError += String.Format("\n{0}", ex.Message);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            System.Diagnostics.Debugger.Break();
                            textError += String.Format("{0} > {1}", nameGroupClass, nameGroup);
                            textError += String.Format("\n{0}", ex.Message);
                        }
                    }
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debugger.Break();
                    textError += String.Format("{0}", nameGroupClass);
                    textError += String.Format("\n{0}", ex.Message);
                }
            }
        }




        /// <summary>
        /// Импорт объектов для справочника "Режущий инструмент" из базы данных системы АСКОН
        /// </summary>
        public void importCT()
        {
            string shortname = "РИ";
            string longname = "Режущий инструмент";
            string textError = "";
            int countItemsAll = 0;
            updateLog(String.Format("Загрузка объектов <{0}> из базы данных Вертикаль.", longname));
            List<Dictionary<string, string>> listLoadObjects = appAsconVertical.getCT();
            updateLog("Загружено " + listLoadObjects.Count + " объектов.");
            List<DinamicParam> listDinamicParams = genListDinamicParams(listLoadObjects);

            updateLog("Синхронизация объектов T-Flex DOCs.");
            // Сортировка по виду инструмента
            foreach (ICollection<Dictionary<string, string>> groupClass in listLoadObjects.GroupBy(x => x["class_GUID"]))
            {
                string nameGroupClass = appData.replaceNameParameterVertical(groupClass.First()["class_NAME"].Trim(' '), shortname);
                string guidGroupClass = groupClass.First()["class_GUID"];
                string guidClass = appData.getGuidTool(shortname, shortname, "Группа");
                string tagGroup = "class_";
                try
                {
                    Dictionary<string, string> parametersGroupClass = getParametersForGroup(groupClass.First(), tagGroup, shortname, nameGroupClass, null);
                    ReferenceObject folderClass = appTflex.createObjectCuttingTool(guidClass, parametersGroupClass);
                    // Сортировка по типу РИ
                    foreach (ICollection<Dictionary<string, string>> group in groupClass.GroupBy(x => x["group_GUID"]))
                    {
                        string nameGroup = group.First()["group_GROUPRI"].Trim(' ');
                        string guidGroup = group.First()["group_GUID"];
                        guidClass = appData.getGuidTool(shortname, shortname, "Группа");
                        tagGroup = "group_";
                        try
                        {
                            Dictionary<string, string> parametersGroup = getParametersForGroup(group.First(), "group_", shortname, nameGroup, parametersGroupClass);
                            foreach (KeyValuePair<string, string> parameter in parametersGroupClass)
                            {
                                if (!parametersGroup.ContainsKey(parameter.Key))
                                {
                                    parametersGroup.Add(parameter.Key, parameter.Value);
                                }
                            }
                            ReferenceObject folder = appTflex.createObjectCuttingTool(guidClass, parametersGroup, folderClass);
                            //
                            // Сортировка по РИ
                            foreach (ICollection<Dictionary<string, string>> group2 in group.GroupBy(x => x["group2_GUID"]))
                            {
                                string nameGroup2 = group2.First()["group2_NAME_RI"].Trim(' ');
                                string guidGroup2 = group2.First()["group2_GUID"];
                                tagGroup = "group2_";
                                try
                                {
                                    // Создание папки "Наименование группы РИ+Стандарт" как дочернию для папки "Наименование типа РИ"
                                    guidClass = appData.getGuidTool(shortname, shortname, "Группа");
                                    Dictionary<string, string> parametersGroup2 = getParametersForGroup(group2.First(), "group2_", shortname, nameGroup2, parametersGroup);
                                    ReferenceObject folder2 = appTflex.createObjectCuttingTool(guidClass, parametersGroup2, folder);
                                    updateLog(String.Format("Синхронизация {0} > {1} > {2}: {3} объектов.", nameGroupClass, nameGroup, nameGroup2, group2.Count()));
                                    int countItems = 0;
                                    foreach (Dictionary<string, string> groupItems in group2)
                                    {
                                        string nameGroupItem = groupItems["item_NAME"].Trim(' '); ;
                                        string guidGroupItem = groupItems["item_GUID"];
                                        guidClass = appData.getGuidTool(shortname, shortname, "Экземпляр");
                                        tagGroup = "item_";
                                        Dictionary<string, string> paramsItems = new Dictionary<string, string>();
                                        try
                                        {
                                            guidClass = appData.getGuidTool(shortname, shortname, "Экземпляр");
                                            paramsItems = getParametersForGroup(groupItems, "item_", shortname, nameGroupItem, parametersGroup2);
                                            foreach (DinamicParam param in listDinamicParams.Where(x => x.guid == groupItems["item_GUID"] && x.fguid == groupItems["item_FGUID"]))
                                            {
                                                foreach (KeyValuePair<string, string> parameterItem in param.parameters)
                                                {
                                                    string name = parameterItem.Key;
                                                    string value = AppData.replaceChars(parameterItem.Value);
                                                    paramsItems.Add(name, value);
                                                }
                                            }
                                            ReferenceObject obj = appTflex.createObjectCuttingTool(guidClass, paramsItems, folder2);
                                            updateProgresBarPre(countItems++, group2.Count(), countItemsAll++, listLoadObjects.Count);
                                        }
                                        catch (Exception ex)
                                        {
                                            System.Diagnostics.Debugger.Break();
                                            textError += String.Format("{0} > {1} > {2} -> {3}", nameGroupClass, nameGroup, nameGroup2, nameGroupItem);
                                            textError += String.Format("\n{0}", ex.Message);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    System.Diagnostics.Debugger.Break();
                                    textError += String.Format("{0} > {1} > {2}", nameGroupClass, nameGroup, nameGroup2);
                                    textError += String.Format("\n{0}", ex.Message);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            System.Diagnostics.Debugger.Break();
                            textError += String.Format("{0} > {1}", nameGroupClass, nameGroup);
                            textError += String.Format("\n{0}", ex.Message);
                        }
                    }
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debugger.Break();
                    textError += String.Format("{0}", nameGroupClass);
                    textError += String.Format("\n{0}", ex.Message);
                }
            }
            updateLog("Синхронизация завершена");
            if (textError != "")
            {
                updateLog("...");
                updateLog("При синхронизации были обнаружены ошибки:");
                updateLog(textError);
            }
            return;
        }
        /// <summary>
        /// Импорт объектов "Режущая часть - пластины" из базы данных системы АСКОН
        /// </summary>
        public void importCB()
        {
            string shortname = "РЧП";
            string textError = "";
            int countAllItems = 0;
            updateLog(String.Format("Загрузка объектов <{0}> из базы данных Вертикаль.", shortname));

            List<Dictionary<string, string>> listLoadObjects = appAsconVertical.getCB();
            updateLog("Загружено " + listLoadObjects.Count + " объектов.");

            List<DinamicParam> listDinamicParams = genListDinamicParams(listLoadObjects);


            updateLog("Синхронизация объектов T-Flex DOCs.");
            foreach (ICollection<Dictionary<string, string>> groupClass in listLoadObjects.GroupBy(x => x["class_GUID"]))
            {
                string nameGroupClass = appData.replaceNameParameterVertical(groupClass.First()["class_NAME"].Trim(' '), shortname);
                string guidGroupClass = groupClass.First()["class_GUID"];
                string guidClass = appData.getGuidTool(shortname, shortname, "Группа");
                string tagGroup = "class_";
                try
                {
                    Dictionary<string, string> parametersGroupClass = getParametersForGroup(groupClass.First(), tagGroup, shortname, nameGroupClass, null);
                    ReferenceObject folderClass = appTflex.createObjectCuttingTool(guidClass, parametersGroupClass);
                    // Сортировка по группе 1
                    foreach (var groupFolder in groupClass.GroupBy(x => x["group_GUID"]))
                    {
                        string nameGroupFolder = groupFolder.First()["group_NAME"].Trim(' ');
                        string guidGroupFolder = groupFolder.First()["group_GUID"];
                        guidClass = appData.getGuidTool(shortname, shortname, "Группа");
                        tagGroup = "group_";
                        try
                        {
                            Dictionary<string, string> parametersGroup = getParametersForGroup(groupFolder.First(), tagGroup, shortname, nameGroupFolder, parametersGroupClass);
                            ReferenceObject folder = appTflex.createObjectCuttingTool(guidClass, parametersGroup, folderClass);
                            // Сортировка по группе 1
                            foreach (var groupFolder2 in groupFolder.GroupBy(x => x["group2_GUID"]))
                            {
                                string nameGroupFolder2 = groupFolder2.First()["group2_OBOZN_ISO"].Trim(' '); ;
                                string guidGroupFolder2 = groupFolder2.First()["group2_GUID"];
                                guidClass = appData.getGuidTool(shortname, shortname, "Группа");
                                tagGroup = "group2_";
                                try
                                {
                                    Dictionary<string, string> parametersGroup2 = getParametersForGroup(groupFolder2.First(), tagGroup, shortname, nameGroupFolder2, parametersGroup);
                                    ReferenceObject folder2 = appTflex.createObjectCuttingTool(guidClass, parametersGroup2, folder);
                                    updateLog(String.Format("Синхронизация {0} > {1} > {2}: {3} объектов.", nameGroupClass, nameGroupFolder, nameGroupFolder2, groupFolder2.Count()));
                                    int countItems = 0;

                                    // Сортировка по экземплярам
                                    foreach (Dictionary<string, string> groupItems in groupFolder2)
                                    {
                                        string nameGroupItem = String.Format("{0} {1}", groupItems["class_NAME"], groupItems["item_OBOZN"]).Trim(' ');
                                        string guidGroupItem = groupItems["item_GUID"];
                                        guidClass = appData.getGuidTool(shortname, shortname, "Экземпляр");
                                        tagGroup = "item_";
                                        Dictionary<string, string> paramsItems = new Dictionary<string, string>();
                                        try
                                        {
                                            paramsItems = getParametersForGroup(groupItems, tagGroup, shortname, nameGroupItem, parametersGroup2);
                                            foreach (DinamicParam param in listDinamicParams.Where(x => x.guid == groupItems["item_GUID"] && x.fguid == groupItems["item_FGUID"]))
                                            {
                                                foreach (KeyValuePair<string, string> parameterItem in param.parameters)
                                                {
                                                    string name = parameterItem.Key;
                                                    string value = AppData.replaceChars(parameterItem.Value).Trim(' ');
                                                    paramsItems.Add(name, value);
                                                }
                                            }
                                            ReferenceObject obj = appTflex.createObjectCuttingTool(guidClass, paramsItems, folder2);
                                            updateProgresBarPre(countItems++, groupFolder2.Count(), countAllItems++, listLoadObjects.Count);
                                        }
                                        catch (Exception ex)
                                        {
                                            System.Diagnostics.Debugger.Break();
                                            textError += String.Format("{0} > {1} > {2} > {3}", nameGroupClass, nameGroupFolder, nameGroupFolder2, nameGroupItem);
                                            textError += String.Format("\n{0}", ex.Message);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    System.Diagnostics.Debugger.Break();
                                    textError += String.Format("{0} > {1} > {2}", nameGroupClass, nameGroupFolder, nameGroupFolder2);
                                    textError += String.Format("\n{0}", ex.Message);
                                }

                            }
                        }
                        catch (Exception ex)
                        {
                            System.Diagnostics.Debugger.Break();
                            textError += String.Format("{0} > {1}", nameGroupClass, nameGroupFolder);
                            textError += String.Format("\n{0}", ex.Message);
                        }
                    }
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debugger.Break();
                    textError += String.Format("{0}", nameGroupClass);
                    textError += String.Format("\n{0}", ex.Message);
                }

            }


            return;
        }
        /// <summary>
        /// Импорт объектов для справочника "Измерительный инструмент" из базы данных системы АСКОН
        /// </summary>
        public void importMI()
        {
            string shortname = "ИИ";
            string textError = "";
            int countAllItems = 0;
            updateLog(String.Format("Загрузка объектов <{0}> из базы данных Вертикаль.", shortname));
            List<Dictionary<string, string>> listLoadObjects = appAsconVertical.getMI();
            updateLog("Загружено " + listLoadObjects.Count + " объектов.");

            List<DinamicParam> listDinamicParams = genListDinamicParams(listLoadObjects);

            // Сортировка по типу
            foreach (ICollection<Dictionary<string, string>> groupClass in listLoadObjects.GroupBy(x => x["class_GUID"]))
            {

                string nameGroupClass = appData.replaceNameParameterVertical(groupClass.First()["class_GROUPII"].Trim(' '), shortname);
                string guidGroupClass = groupClass.First()["class_GUID"];
                string guidClass = appData.getGuidTool(shortname, shortname, "Группа");
                string tagGroup = "class_";
                try
                {
                    Dictionary<string, string> parametersGroupClass = getParametersForGroup(groupClass.First(), tagGroup, shortname, nameGroupClass, null);
                    ReferenceObject folderClass = appTflex.createObjectCuttingTool(guidClass, parametersGroupClass);
                    foreach (ICollection<Dictionary<string, string>> groupFolder in groupClass.GroupBy(x => x["group_GUID"]))
                    {
                        string nameGroupFolder = groupFolder.First()["group_NAME_II"].Trim(' ');
                        string guidGroupFolder = groupFolder.First()["group_GUID"];
                        guidClass = appData.getGuidTool(shortname, shortname, "Группа");
                        tagGroup = "group_";
                        try
                        {
                            Dictionary<string, string> parametersGroup = getParametersForGroup(groupFolder.First(), tagGroup, shortname, nameGroupFolder, parametersGroupClass);
                            foreach (KeyValuePair<string, string> parameter in parametersGroupClass)
                            {
                                if (!parametersGroup.ContainsKey(parameter.Key))
                                {
                                    parametersGroup.Add(parameter.Key, parameter.Value);
                                }
                            }
                            ReferenceObject folder = appTflex.createObjectCuttingTool(guidClass, parametersGroup, folderClass);
                            // Сортировка по экземплярам
                            updateLog(String.Format("Синхронизация {0} > {1} > {2} объектов.", nameGroupClass, nameGroupFolder, groupFolder.Count()));
                            int countItems = 0;
                            foreach (Dictionary<string, string> groupItems in groupFolder)
                            {
                                string nameGroupItem = String.Format("{0}", groupItems["item_NAME"]).Trim(' ');
                                string guidGroupItem = groupItems["item_GUID"];
                                guidClass = appData.getGuidTool(shortname, shortname, "Экземпляр");
                                tagGroup = "item_";
                                try
                                {
                                    Dictionary<string, string> paramsItems = getParametersForGroup(groupItems, tagGroup, shortname, nameGroupItem, parametersGroup);
                                    foreach (DinamicParam param in listDinamicParams.Where(x => x.guid == groupItems["item_GUID"] && x.fguid == groupItems["item_FGUID"]))
                                    {
                                        foreach (KeyValuePair<string, string> parameterItem in param.parameters)
                                        {
                                            string name = parameterItem.Key;
                                            string value = AppData.replaceChars(parameterItem.Value).Trim(' ');
                                            paramsItems.Add(name, value);
                                        }
                                    }
                                    ReferenceObject obj = appTflex.createObjectCuttingTool(guidClass, paramsItems, folder);
                                    updateProgresBarPre(countItems++, groupFolder.Count(), countAllItems++, listLoadObjects.Count);
                                }
                                catch (Exception ex)
                                {
                                    textError += String.Format("{0} > {1} > {2} ", nameGroupClass, nameGroupFolder, nameGroupItem);
                                    textError += String.Format("\n{0}", ex.Message);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            System.Diagnostics.Debugger.Break();
                            textError += String.Format("{0} > {1}", nameGroupClass, nameGroupFolder);
                            textError += String.Format("\n{0}", ex.Message);
                        }
                    }
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debugger.Break();
                    textError += String.Format("{0}", nameGroupClass);
                    textError += String.Format("\n{0}", ex.Message);
                }
            }
            updateProgresBarPre(1, 1, 1, 1);
            MessageBox.Show("Готово!");
        }
        /// <summary>
        /// Импорт объектов "Штамповочная оснастка" из базы данных системы АСКОН
        /// </summary>
        public void importSE()
        {
            string shortname = "ШО";
            string textError = "";
            int countAllItems = 0;
            updateLog(String.Format("Загрузка объектов <{0}> из базы данных Вертикаль.", shortname));

            List<Dictionary<string, string>> listLoadObjects = appAsconVertical.getSE();
            updateLog("Загружено " + listLoadObjects.Count + " объектов.");

            List<DinamicParam> listDinamicParams = genListDinamicParams(listLoadObjects);

            updateLog("Синхронизация объектов T-Flex DOCs.");
            foreach (ICollection<Dictionary<string, string>> groupClass in listLoadObjects.GroupBy(x => x["class_GUID"]))
            {
                string nameGroupClass = appData.replaceNameParameterVertical(groupClass.First()["class_A"].Trim(' '), shortname);
                string guidGroupClass = groupClass.First()["class_GUID"];
                string guidClass = appData.getGuidTool(shortname, shortname, "Группа");
                string tagGroup = "class_";
                try
                {
                    Dictionary<string, string> parametersGroupClass = getParametersForGroup(groupClass.First(), tagGroup, shortname, nameGroupClass, null);
                    ReferenceObject folderClass = appTflex.createObjectCuttingTool(guidClass, parametersGroupClass);
                    foreach (ICollection<Dictionary<string, string>> groupFolder in groupClass.GroupBy(x => x["group_GUID"]))
                    {
                        string nameGroupFolder = groupFolder.First()["group_NAMEGROSN"].Trim(' ');
                        string guidGroupFolder = groupFolder.First()["group_GUID"];
                        guidClass = appData.getGuidTool(shortname, shortname, "Группа");
                        tagGroup = "group_";
                        try
                        {
                            Dictionary<string, string> parametersGroup = getParametersForGroup(groupFolder.First(), tagGroup, shortname, nameGroupFolder, parametersGroupClass);
                            ReferenceObject folder = appTflex.createObjectCuttingTool(guidClass, parametersGroup, folderClass);
                            updateLog(String.Format("Синхронизация {0} > {1} > {2} объектов.", nameGroupClass, nameGroupFolder, groupFolder.Count()));
                            int countItems = 0;
                            foreach (Dictionary<string, string> groupItems in groupFolder)
                            {
                                string nameGroupItem = String.Format("{0}", groupItems["item_NAMEOSN"]).Trim(' ');
                                string guidGroupItem = groupItems["item_GUID"];
                                guidClass = appData.getGuidTool(shortname, shortname, "Экземпляр");
                                tagGroup = "item_";
                                try
                                {
                                    Dictionary<string, string> paramsItems = getParametersForGroup(groupItems, tagGroup, shortname, nameGroupItem, parametersGroup);
                                    foreach (DinamicParam param in listDinamicParams.Where(x => x.guid == groupItems["item_GUID"] && x.fguid == groupItems["item_FGUID"]))
                                    {
                                        foreach (KeyValuePair<string, string> parameterItem in param.parameters)
                                        {
                                            string name = parameterItem.Key;
                                            string value = AppData.replaceChars(parameterItem.Value).Trim(' ');
                                            paramsItems.Add(name, value);
                                        }
                                    }
                                    ReferenceObject obj = appTflex.createObjectCuttingTool(guidClass, paramsItems, folder);
                                    updateProgresBarPre(countItems++, groupFolder.Count(), countAllItems++, listLoadObjects.Count);
                                }
                                catch (Exception ex)
                                {
                                    textError += String.Format("{0} > {1} > {2}", nameGroupClass, nameGroupFolder, nameGroupItem);
                                    textError += String.Format("\n{0}", ex.Message);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            System.Diagnostics.Debugger.Break();
                            textError += String.Format("{0} > {1}", nameGroupClass, nameGroupFolder);
                            textError += String.Format("\n{0}", ex.Message);
                        }

                    }
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debugger.Break();
                    textError += String.Format("{0}", nameGroupClass);
                    textError += String.Format("\n{0}", ex.Message);
                    continue;
                }

            }
        }
        /// <summary>
        /// Импорт объектов "Штамповочная оснастка" из базы данных системы АСКОН
        /// </summary>
        public void importSHI()
        {
            string shortname = "ШИ";
            string textError = "";
            int countAllItems = 0;
            updateLog(String.Format("Загрузка объектов <{0}> из базы данных Вертикаль.", shortname));

            List<Dictionary<string, string>> listLoadObjects = appAsconVertical.getSI();
            updateLog("Загружено " + listLoadObjects.Count + " объектов.");

            List<DinamicParam> listDinamicParams = genListDinamicParams(listLoadObjects);

            updateLog("Синхронизация объектов T-Flex DOCs.");
            foreach (ICollection<Dictionary<string, string>> groupClass in listLoadObjects.GroupBy(x => x["class_GUID"]))
            {
                string nameGroupClass = appData.replaceNameParameterVertical(groupClass.First()["class_A"].Trim(' '), shortname);
                string guidGroupClass = groupClass.First()["class_GUID"];
                string guidClass = appData.getGuidTool(shortname, shortname, "Группа");
                string tagGroup = "class_";
                try
                {
                    Dictionary<string, string> parametersGroupClass = getParametersForGroup(groupClass.First(), tagGroup, shortname, nameGroupClass, null);
                    ReferenceObject folderClass = appTflex.createObjectCuttingTool(guidClass, parametersGroupClass);

                    foreach (ICollection<Dictionary<string, string>> groupFolder in groupClass.GroupBy(x => x["group_GUID"]))
                    {
                        string nameGroupFolder = groupFolder.First()["group_NAMEGRINS"].Trim(' ');
                        string guidGroupFolder = groupFolder.First()["group_GUID"];
                        guidClass = appData.getGuidTool(shortname, shortname, "Группа");
                        tagGroup = "group_";
                        try
                        {
                            Dictionary<string, string> parametersGroup = getParametersForGroup(groupFolder.First(), tagGroup, shortname, nameGroupFolder, parametersGroupClass);
                            ReferenceObject folder = appTflex.createObjectCuttingTool(guidClass, parametersGroup, folderClass);
                            updateLog(String.Format("Синхронизация {0} > {1} > {2} объектов.", nameGroupClass, nameGroupFolder, groupFolder.Count()));
                            int countItems = 0;
                            foreach (Dictionary<string, string> groupItems in groupFolder)
                            {
                                string nameGroupItem = String.Format("{0}", groupItems["item_NAMEINSTR"]).Trim(' ');
                                string guidGroupItem = groupItems["item_GUID"];
                                guidClass = appData.getGuidTool(shortname, shortname, "Экземпляр");
                                tagGroup = "item_";
                                Dictionary<string, string> paramsItems = new Dictionary<string, string>();
                                try
                                {
                                    paramsItems = getParametersForGroup(groupItems, tagGroup, shortname, nameGroupItem, parametersGroup);
                                    foreach (DinamicParam param in listDinamicParams.Where(x => x.guid == groupItems["item_GUID"] && x.fguid == groupItems["item_FGUID"]))
                                    {
                                        foreach (KeyValuePair<string, string> parameterItem in param.parameters)
                                        {
                                            string name = parameterItem.Key;
                                            string value = AppData.replaceChars(parameterItem.Value).Trim(' ');
                                            paramsItems.Add(name, value);
                                        }
                                    }
                                    ReferenceObject obj = appTflex.createObjectCuttingTool(guidClass, paramsItems, folder);
                                    updateProgresBarPre(countItems++, groupFolder.Count(), countAllItems++, listLoadObjects.Count);
                                }
                                catch (Exception ex)
                                {
                                    textError += String.Format("{0} > {1} > {2}", nameGroupClass, nameGroupFolder, nameGroupItem);
                                    textError += String.Format("\n{0}", ex.Message);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            System.Diagnostics.Debugger.Break();
                            textError += String.Format("{0} > {1}", nameGroupClass, nameGroupFolder);
                            textError += String.Format("\n{0}", ex.Message);
                        }

                    }
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debugger.Break();
                    textError += String.Format("{0}", nameGroupClass);
                    textError += String.Format("\n{0}", ex.Message);
                    continue;
                }
            }
            updateProgresBarPre(1, 1, 1, 1);
            MessageBox.Show("Готово!");
        }

        /// <summary>
        /// Импорт объектов "Станочные приспособления" из базы данных системы АСКОН
        /// </summary>
        public void importSP()
        {
            string shortname = "СП";
            string longname = "Станочные приспособления";
            string textError = "";
            int countAllItems = 0;
            updateLog(String.Format("Загрузка объектов <{0}> из базы данных Вертикаль.", longname));
            List<Dictionary<string, string>> listLoadObjects = appAsconVertical.getSP();
            updateLog("Загружено " + listLoadObjects.Count + " объектов.");

            List<DinamicParam> listDinamicParams = genListDinamicParams(listLoadObjects);

            updateLog("Синхронизация объектов T-Flex DOCs.");
            //
            // Сортировка по class_GUID
            foreach (ICollection<Dictionary<string, string>> groupClass in listLoadObjects.GroupBy(x => x["class_GUID"]))
            {
                string nameGroupClass = appData.replaceNameParameterVertical(groupClass.First()["class_VID_PRISP"].Trim(' '), shortname);
                string guidGroupClass = groupClass.First()["class_GUID"];
                string guidClass = appData.getGuidTool(shortname, shortname, "Группа");
                string tagGroup = "class_";
                try
                {
                    Dictionary<string, string> parametersGroupClass = getParametersForGroup(groupClass.First(), tagGroup, shortname, nameGroupClass, null);
                    ReferenceObject folderClass = appTflex.createObjectCuttingTool(guidClass, parametersGroupClass);
                    //
                    // Сортировка по group_GUID
                    foreach (ICollection<Dictionary<string, string>> groupFolder in groupClass.GroupBy(x => x["group_GUID"]))
                    {
                        string nameGroupFolder = groupFolder.First()["group_A"].Trim(' ');
                        string guidGroupFolder = groupFolder.First()["group_GUID"];
                        guidClass = appData.getGuidTool(shortname, shortname, "Группа");
                        tagGroup = "group_";
                        try
                        {
                            Dictionary<string, string> parametersGroup = getParametersForGroup(groupFolder.First(), tagGroup, shortname, nameGroupFolder, parametersGroupClass);
                            ReferenceObject folder = appTflex.createObjectCuttingTool(guidClass, parametersGroup, folderClass);
                            //
                            // Сортировка по group2_GUID
                            foreach (ICollection<Dictionary<string, string>> groupFolder2 in groupFolder.GroupBy(x => x["group2_GUID"]))
                            {
                                string nameGroupFolder2 = groupFolder.First()["group2_NAME_PRISP"].Trim(' ');
                                string guidGroupFolder2 = groupFolder.First()["group2_GUID"];
                                guidClass = appData.getGuidTool(shortname, shortname, "Группа");
                                tagGroup = "group2_";
                                try
                                {
                                    Dictionary<string, string> parametersGroup2 = getParametersForGroup(groupFolder2.First(), tagGroup, shortname, nameGroupFolder2, parametersGroup);
                                    ReferenceObject folder2 = appTflex.createObjectCuttingTool(guidClass, parametersGroup2, folderClass);
                                    updateLog(String.Format("Синхронизация {0} > {1} > {2} > {3} объектов.", nameGroupClass, nameGroupFolder, nameGroupFolder2, groupFolder2.Count()));
                                    int countItems = 0;
                                    foreach (Dictionary<string, string> groupItems in groupFolder2)
                                    {
                                        string nameGroupItem = String.Format("{0} {1}", nameGroupFolder2, groupItems["item_OBOZN"]).Trim(' ');
                                        string guidGroupItem = groupItems["item_GUID"];
                                        guidClass = appData.getGuidTool(shortname, shortname, "Экземпляр");
                                        tagGroup = "item_";
                                        try
                                        {
                                            Dictionary<string, string> paramsItems = getParametersForGroup(groupItems, tagGroup, shortname, nameGroupItem, parametersGroup2);
                                            foreach (DinamicParam param in listDinamicParams.Where(x => x.guid == groupItems["item_GUID"] && x.fguid == groupItems["item_FGUID"]))
                                            {
                                                foreach (KeyValuePair<string, string> parameterItem in param.parameters)
                                                {
                                                    string name = parameterItem.Key;
                                                    string value = AppData.replaceChars(parameterItem.Value).Trim(' ');
                                                    paramsItems.Add(name, value);
                                                }
                                            }
                                            ReferenceObject obj = appTflex.createObjectCuttingTool(guidClass, paramsItems, folder2);
                                            updateProgresBarPre(countItems++, groupFolder.Count(), countAllItems++, listLoadObjects.Count);
                                        }
                                        catch (Exception ex)
                                        {
                                            textError += String.Format("{0} > {1} > {2} ", nameGroupClass, nameGroupFolder, nameGroupItem);
                                            textError += String.Format("\n{0}", ex.Message);
                                        }
                                    }

                                }
                                catch (Exception ex)
                                {
                                    System.Diagnostics.Debugger.Break();
                                    textError += String.Format("{0} > {1} > {2}", nameGroupClass, nameGroupFolder, nameGroupFolder2);
                                    textError += String.Format("\n{0}", ex.Message);
                                }

                            }

                        }
                        catch (Exception ex)
                        {
                            System.Diagnostics.Debugger.Break();
                            textError += String.Format("{0} > {1}", nameGroupClass, nameGroupFolder);
                            textError += String.Format("\n{0}", ex.Message);
                        }
                    }
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debugger.Break();
                    textError += String.Format("{0}", nameGroupClass);
                    textError += String.Format("\n{0}", ex.Message);
                }
            }
            updateProgresBarPre(1, 1, 1, 1);
            MessageBox.Show("Готово!");

        }
        /// <summary>
        /// Импорт объектов для справочника "Оснастка лакокраски" из базы данных системы АСКОН
        /// </summary>
        public void importRL()
        {
            string shortname = "ОЛ";
            string longname = "Оснастка лакокраски";
            string textError = "";
            int countAllItems = 0;
            updateLog(String.Format("Загрузка объектов <{0}> из базы данных Вертикаль.", longname));
            List<Dictionary<string, string>> listLoadObjects = appAsconVertical.getRL();
            updateLog("Загружено " + listLoadObjects.Count + " объектов.");

            List<DinamicParam> listDinamicParams = genListDinamicParams(listLoadObjects);

            updateLog("Синхронизация объектов T-Flex DOCs.");

            foreach (ICollection<Dictionary<string, string>> groupClass in listLoadObjects.GroupBy(x => x["class_GUID"]))
            {
                string nameGroupClass = appData.replaceNameParameterVertical(groupClass.First()["class_VID"].Trim(' '), shortname);
                string guidGroupClass = groupClass.First()["class_GUID"];
                string guidClass = appData.getGuidTool(shortname, shortname, "Группа");
                string tagGroup = "class_";
                try
                {
                    Dictionary<string, string> parametersGroupClass = getParametersForGroup(groupClass.First(), tagGroup, shortname, nameGroupClass, null);
                    ReferenceObject folderClass = appTflex.createObjectCuttingTool(guidClass, parametersGroupClass);
                    updateLog(String.Format("Синхронизация {0} > {1} объектов.", nameGroupClass, groupClass.Count()));
                    int countItems = 0;
                    foreach (Dictionary<string, string> groupItems in groupClass)
                    {
                        string nameGroupItem = String.Format("{0}", groupItems["item_NAMEINSR"]).Trim(' ');
                        string guidGroupItem = groupItems["item_GUID"];
                        guidClass = appData.getGuidTool(shortname, shortname, "Экземпляр");
                        tagGroup = "item_";
                        try
                        {
                            Dictionary<string, string> paramsItems = getParametersForGroup(groupItems, tagGroup, shortname, nameGroupItem, parametersGroupClass);
                            foreach (DinamicParam param in listDinamicParams.Where(x => x.guid == groupItems["item_GUID"] && x.fguid == groupItems["item_FGUID"]))
                            {
                                foreach (KeyValuePair<string, string> parameterItem in param.parameters)
                                {
                                    string name = parameterItem.Key;
                                    string value = AppData.replaceChars(parameterItem.Value).Trim(' ');
                                    paramsItems.Add(name, value);
                                }
                            }
                            ReferenceObject obj = appTflex.createObjectCuttingTool(guidClass, paramsItems, folderClass);
                            updateProgresBarPre(countItems++, groupClass.Count(), countAllItems++, listLoadObjects.Count);
                        }
                        catch (Exception ex)
                        {
                            System.Diagnostics.Debugger.Break();
                            textError += String.Format("{0} > {1} ", nameGroupClass, nameGroupItem);
                            textError += String.Format("\n{0}", ex.Message);
                        }
                    }
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debugger.Break();
                    textError += String.Format("{0}", nameGroupClass);
                    textError += String.Format("\n{0}", ex.Message);
                }
            }

            updateProgresBarPre(1, 1, 1, 1);
            MessageBox.Show("Готово!");
        }
        /// <summary>
        /// Импорт объектов "Оснастка термообработки" из базы данных системы АСКОН
        /// </summary>
        public void importRT()
        {
            string shortname = "ОТ";
            string longname = "Оснастка термообработки";
            string textError = "";
            int countAllItems = 0;
            updateLog(String.Format("Загрузка объектов <{0}> из базы данных Вертикаль.", longname));
            List<Dictionary<string, string>> listLoadObjects = appAsconVertical.getRT();
            updateLog("Загружено " + listLoadObjects.Count + " объектов.");

            List<DinamicParam> listDinamicParams = genListDinamicParams(listLoadObjects);

            updateLog("Синхронизация объектов T-Flex DOCs.");

            foreach (ICollection<Dictionary<string, string>> groupClass in listLoadObjects.GroupBy(x => x["class_GUID"]))
            {
                string nameGroupClass = appData.replaceNameParameterVertical(groupClass.First()["class_NAMEVID"].Trim(' '), shortname);
                string guidGroupClass = groupClass.First()["class_GUID"];
                string guidClass = appData.getGuidTool(shortname, shortname, "Группа");
                string tagGroup = "class_";
                try
                {
                    Dictionary<string, string> parametersGroupClass = getParametersForGroup(groupClass.First(), tagGroup, shortname, nameGroupClass, null);
                    ReferenceObject folderClass = appTflex.createObjectCuttingTool(guidClass, parametersGroupClass);
                    foreach (ICollection<Dictionary<string, string>> groupFolder in groupClass.GroupBy(x => x["group_GUID"]))
                    {
                        string nameGroupFolder = groupFolder.First()["group_NAMETIPOSNTRM"].Trim(' ');
                        string guidGroupFolder = groupFolder.First()["group_GUID"];
                        guidClass = appData.getGuidTool(shortname, shortname, "Группа");
                        tagGroup = "group_";
                        try
                        {
                            Dictionary<string, string> parametersGroup = getParametersForGroup(groupFolder.First(), tagGroup, shortname, nameGroupFolder, parametersGroupClass);
                            ReferenceObject folder = appTflex.createObjectCuttingTool(guidClass, parametersGroup, folderClass);
                            updateLog(String.Format("Синхронизация {0} > {1} > {2} > {3} объектов.", nameGroupClass, nameGroupFolder, nameGroupFolder, groupFolder.Count()));
                            int countItems = 0;
                            foreach (Dictionary<string, string> groupItems in groupFolder)
                            {
                                string nameGroupItem = groupItems["item_NAMEOSN"].Trim(' ');
                                string guidGroupItem = groupItems["item_GUID"];
                                guidClass = appData.getGuidTool(shortname, shortname, "Экземпляр");
                                tagGroup = "item_";
                                try
                                {
                                    Dictionary<string, string> paramsItems = getParametersForGroup(groupItems, tagGroup, shortname, nameGroupItem, parametersGroup);
                                    foreach (DinamicParam param in listDinamicParams.Where(x => x.guid == groupItems["item_GUID"] && x.fguid == groupItems["item_FGUID"]))
                                    {
                                        foreach (KeyValuePair<string, string> parameterItem in param.parameters)
                                        {
                                            string name = parameterItem.Key;
                                            string value = AppData.replaceChars(parameterItem.Value).Trim(' ');
                                            paramsItems.Add(name, value);
                                        }
                                    }
                                    ReferenceObject obj = appTflex.createObjectCuttingTool(guidClass, paramsItems, folder);
                                    updateProgresBarPre(countItems++, groupFolder.Count(), countAllItems++, listLoadObjects.Count);
                                }
                                catch (Exception ex)
                                {
                                    System.Diagnostics.Debugger.Break();
                                    textError += String.Format("{0} > {1} > {2} ", nameGroupClass, nameGroupFolder, nameGroupItem);
                                    textError += String.Format("\n{0}", ex.Message);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            System.Diagnostics.Debugger.Break();
                            textError += String.Format("{0} > {1}", nameGroupClass, nameGroupFolder);
                            textError += String.Format("\n{0}", ex.Message);
                        }

                    }

                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debugger.Break();
                    textError += String.Format("{0}", nameGroupClass);
                    textError += String.Format("\n{0}", ex.Message);
                }
            }

            updateProgresBarPre(1, 1, 1, 1);
            MessageBox.Show("Готово!");
        }

        /// <summary>
        /// Импорт объектов "Приборы" из базы данных системы АСКОН
        /// </summary>
        public void importP()
        {
            string shortname = "Приборы";
            string longname = "Приборы";
            string textError = "";
            int countAllItems = 0;
            updateLog(String.Format("Загрузка объектов <{0}> из базы данных Вертикаль.", longname));
            List<Dictionary<string, string>> listLoadObjects = appAsconVertical.getP();
            updateLog("Загружено " + listLoadObjects.Count + " объектов.");

            List<DinamicParam> listDinamicParams = genListDinamicParams(listLoadObjects);

            updateLog("Синхронизация объектов T-Flex DOCs.");

            foreach (ICollection<Dictionary<string, string>> groupClass in listLoadObjects.GroupBy(x => x["class_GUID"]))
            {
                string nameGroupClass = appData.replaceNameParameterVertical(groupClass.First()["class_NAME"].Trim(' '), shortname);
                string guidGroupClass = groupClass.First()["class_GUID"];
                string guidClass = appData.getGuidTool(shortname, shortname, "Группа");
                string tagGroup = "class_";
                try
                {
                    Dictionary<string, string> parametersGroupClass = getParametersForGroup(groupClass.First(), tagGroup, shortname, nameGroupClass, null);
                    ReferenceObject folderClass = appTflex.createObjectCuttingTool(guidClass, parametersGroupClass);
                    updateLog(String.Format("Синхронизация {0} > {1} объектов.", nameGroupClass, groupClass.Count()));
                    int countItems = 0;
                    foreach (Dictionary<string, string> groupItems in groupClass)
                    {
                        string nameGroupItem = groupItems["item_NAME"].Trim(' ');
                        string guidGroupItem = groupItems["item_GUID"];
                        guidClass = appData.getGuidTool(shortname, shortname, "Экземпляр");
                        tagGroup = "item_";
                        try
                        {
                            Dictionary<string, string> paramsItems = getParametersForGroup(groupItems, tagGroup, shortname, nameGroupItem, parametersGroupClass);
                            foreach (DinamicParam param in listDinamicParams.Where(x => x.guid == groupItems["item_GUID"] && x.fguid == groupItems["item_FGUID"]))
                            {
                                foreach (KeyValuePair<string, string> parameterItem in param.parameters)
                                {
                                    string name = parameterItem.Key;
                                    string value = AppData.replaceChars(parameterItem.Value).Trim(' ');
                                    paramsItems.Add(name, value);
                                }
                            }
                            ReferenceObject obj = appTflex.createObjectCuttingTool(guidClass, paramsItems, folderClass);
                            updateProgresBarPre(countItems++, folderClass.Count(), countAllItems++, listLoadObjects.Count);
                        }
                        catch (Exception ex)
                        {
                            textError += String.Format("{0} > {1} ", nameGroupClass, nameGroupItem);
                            textError += String.Format("\n{0}", ex.Message);
                        }

                    }

                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debugger.Break();
                    textError += String.Format("{0}", nameGroupClass);
                    textError += String.Format("\n{0}", ex.Message);
                }

            }


            updateProgresBarPre(1, 1, 1, 1);
            MessageBox.Show("Готово!");
        }

        /// <summary>
        /// 
        /// </summary>
        public void importSBO()
        {
            string shortname = "СБО";
            string longname = "Сборочная оснастка";
            string textError = "";
            int countAllItems = 0;
            updateLog(String.Format("Загрузка объектов <{0}> из базы данных Вертикаль.", longname));
            List<Dictionary<string, string>> listLoadObjects = appAsconVertical.getSBO();
            updateLog("Загружено " + listLoadObjects.Count + " объектов.");

            List<DinamicParam> listDinamicParams = genListDinamicParams(listLoadObjects);

            updateLog("Синхронизация объектов T-Flex DOCs.");

            foreach (ICollection<Dictionary<string, string>> groupClass in listLoadObjects.GroupBy(x => x["class_GUID"]))
            {
                string nameGroupClass = groupClass.First()["class_A"].Trim(' ');
                string guidGroupClass = groupClass.First()["class_GUID"];
                string guidClass = appData.getGuidTool(shortname, shortname, "Группа");
                string tagGroup = "class_";
                try
                {
                    Dictionary<string, string> parametersGroupClass = getParametersForGroup(groupClass.First(), tagGroup, shortname, nameGroupClass, null);
                    ReferenceObject folderClass = appTflex.createObjectCuttingTool(guidClass, parametersGroupClass);
                    foreach (ICollection<Dictionary<string, string>> groupFolder in groupClass.GroupBy(x => x["group_GUID"]))
                    {
                        string nameGroupFolder = groupFolder.First()["group_OSNASGR"].Trim(' ');
                        string guidGroupFolder = groupFolder.First()["group_GUID"];
                        guidClass = appData.getGuidTool(shortname, shortname, "Группа");
                        tagGroup = "group_";
                        try
                        {
                            Dictionary<string, string> parametersGroup = getParametersForGroup(groupFolder.First(), tagGroup, shortname, nameGroupFolder, parametersGroupClass);
                            ReferenceObject folder = appTflex.createObjectCuttingTool(guidClass, parametersGroup, folderClass);
                            updateLog(String.Format("Синхронизация {0} > {1} > {2} объектов.", nameGroupClass, nameGroupFolder, groupFolder.Count()));
                            int countItems = 0;
                            foreach (Dictionary<string, string> groupItems in groupFolder)
                            {
                                string nameGroupItem = groupItems["item_NAMEOSN"].Trim(' ');
                                string guidGroupItem = groupItems["item_GUID"];
                                guidClass = appData.getGuidTool(shortname, shortname, "Экземпляр");
                                tagGroup = "item_";
                                try
                                {
                                    Dictionary<string, string> paramsItems = getParametersForGroup(groupItems, tagGroup, shortname, nameGroupItem, parametersGroup);
                                    foreach (DinamicParam param in listDinamicParams.Where(x => x.guid == groupItems["item_GUID"] && x.fguid == groupItems["item_FGUID"]))
                                    {
                                        foreach (KeyValuePair<string, string> parameterItem in param.parameters)
                                        {
                                            string name = parameterItem.Key;
                                            string value = AppData.replaceChars(parameterItem.Value).Trim(' ');
                                            paramsItems.Add(name, value);
                                        }
                                    }
                                    ReferenceObject obj = appTflex.createObjectCuttingTool(guidClass, paramsItems, folder);
                                    updateProgresBarPre(countItems++, groupFolder.Count(), countAllItems++, listLoadObjects.Count);
                                }
                                catch (Exception ex)
                                {
                                    textError += String.Format("{0} > {1} > {2} ", nameGroupClass, nameGroupFolder, nameGroupItem);
                                    textError += String.Format("\n{0}", ex.Message);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            System.Diagnostics.Debugger.Break();
                            textError += String.Format("{0} > {1}", nameGroupClass, nameGroupFolder);
                            textError += String.Format("\n{0}", ex.Message);
                        }
                    }
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debugger.Break();
                    textError += String.Format("{0}", nameGroupClass);
                    textError += String.Format("\n{0}", ex.Message);
                }

            }
            updateProgresBarPre(1, 1, 1, 1);
            MessageBox.Show("Готово!");
        }


        /// <summary>
        /// 
        /// </summary>
        public void importSVO()
        {
            string shortname = "СВО";
            string longname = "Сварочная оснастка";
            string textError = "";
            int countAllItems = 0;
            updateLog(String.Format("Загрузка объектов <{0}> из базы данных Вертикаль.", longname));
            List<Dictionary<string, string>> listLoadObjects = appAsconVertical.getSVO();
            updateLog("Загружено " + listLoadObjects.Count + " объектов.");

            List<DinamicParam> listDinamicParams = genListDinamicParams(listLoadObjects);

            updateLog("Синхронизация объектов T-Flex DOCs.");

            foreach (ICollection<Dictionary<string, string>> groupClass in listLoadObjects.GroupBy(x => x["class_GUID"]))
            {
                string nameGroupClass = groupClass.First()["class_A"].Trim(' ');
                string guidGroupClass = groupClass.First()["class_GUID"];
                string guidClass = appData.getGuidTool(shortname, shortname, "Группа");
                string tagGroup = "class_";
                try
                {
                    Dictionary<string, string> parametersGroupClass = getParametersForGroup(groupClass.First(), tagGroup, shortname, nameGroupClass, null);
                    ReferenceObject folderClass = appTflex.createObjectCuttingTool(guidClass, parametersGroupClass);
                    updateLog(String.Format("Синхронизация {0} > {1}  объектов.", nameGroupClass, folderClass.Count()));
                    int countItems = 0;
                    foreach (Dictionary<string, string> groupItems in groupClass)
                    {
                        string nameGroupItem = groupItems["item_NAMEOSN"].Trim(' ');
                        string guidGroupItem = groupItems["item_GUID"];
                        guidClass = appData.getGuidTool(shortname, shortname, "Экземпляр");
                        tagGroup = "item_";
                        try
                        {
                            Dictionary<string, string> paramsItems = getParametersForGroup(groupItems, tagGroup, shortname, nameGroupItem, parametersGroupClass);
                            foreach (DinamicParam param in listDinamicParams.Where(x => x.guid == groupItems["item_GUID"] && x.fguid == groupItems["item_FGUID"]))
                            {
                                foreach (KeyValuePair<string, string> parameterItem in param.parameters)
                                {
                                    string name = parameterItem.Key;
                                    string value = AppData.replaceChars(parameterItem.Value).Trim(' ');
                                    paramsItems.Add(name, value);
                                }
                            }
                            ReferenceObject obj = appTflex.createObjectCuttingTool(guidClass, paramsItems, folderClass);
                            updateProgresBarPre(countItems++, groupClass.Count(), countAllItems++, listLoadObjects.Count);
                        }
                        catch (Exception ex)
                        {
                            textError += String.Format("{0} > {1} ", nameGroupClass, nameGroupItem);
                            textError += String.Format("\n{0}", ex.Message);
                        }
                    }
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debugger.Break();
                    textError += String.Format("{0}", nameGroupClass);
                    textError += String.Format("\n{0}", ex.Message);
                }

            }
            updateProgresBarPre(1, 1, 1, 1);
            MessageBox.Show("Готово!");
        }

        /// <summary>
        /// 
        /// </summary>
        public void importSI()
        {
            string shortname = "СИ";
            string longname = "Слесарный инструмент";
            string textError = "";
            int countAllItems = 0;
            updateLog(String.Format("Загрузка объектов <{0}> из базы данных Вертикаль.", longname));
            List<Dictionary<string, string>> listLoadObjects = appAsconVertical.getSI();
            updateLog("Загружено " + listLoadObjects.Count + " объектов.");

            List<DinamicParam> listDinamicParams = genListDinamicParams(listLoadObjects);

            updateLog("Синхронизация объектов T-Flex DOCs.");

            foreach (ICollection<Dictionary<string, string>> groupClass in listLoadObjects.GroupBy(x => x["class_GUID"]))
            {
                string nameGroupClass = groupClass.First()["class_A"].Trim(' ');
                string guidGroupClass = groupClass.First()["class_GUID"];
                string guidClass = appData.getGuidTool(shortname, shortname, "Группа");
                string tagGroup = "class_";
                try
                {
                    Dictionary<string, string> parametersGroupClass = getParametersForGroup(groupClass.First(), tagGroup, shortname, nameGroupClass, null);
                    ReferenceObject folderClass = appTflex.createObjectCuttingTool(guidClass, parametersGroupClass);
                    foreach (ICollection<Dictionary<string, string>> groupFolder in groupClass.GroupBy(x => x["group_GUID"]))
                    {
                        string nameGroupFolder = String.Format("{0} {1}", groupFolder.First()["group_NAMEGRINS"], groupFolder.First()["group_KODTIPINS"]).Trim(' ');
                        string guidGroupFolder = groupFolder.First()["group_GUID"];
                        guidClass = appData.getGuidTool(shortname, shortname, "Группа");
                        tagGroup = "group_";
                        try
                        {
                            Dictionary<string, string> parametersGroup = getParametersForGroup(groupFolder.First(), tagGroup, shortname, nameGroupFolder, parametersGroupClass);
                            ReferenceObject folder = appTflex.createObjectCuttingTool(guidClass, parametersGroup, folderClass);
                            foreach (ICollection<Dictionary<string, string>> groupFolder2 in groupClass.GroupBy(x => x["group2_GUID"]))
                            {
                                string nameGroupFolder2 = String.Format("{0} {1}", groupFolder.First()["group2_NAMEINSTR"], groupFolder.First()["group2_INSRTSHT"]).Trim(' ');
                                string guidGroupFolder2 = groupFolder.First()["group_GUID"];
                                guidClass = appData.getGuidTool(shortname, shortname, "Группа");
                                tagGroup = "group2_";
                                try
                                {
                                    Dictionary<string, string> parametersGroup2 = getParametersForGroup(groupFolder2.First(), tagGroup, shortname, nameGroupFolder2, parametersGroup);
                                    ReferenceObject folder2 = appTflex.createObjectCuttingTool(guidClass, parametersGroup, folder);
                                    updateLog(String.Format("Синхронизация {0} > {1} > {2} объектов.", nameGroupClass, nameGroupFolder, groupFolder.Count()));
                                    int countItems = 0;
                                    foreach (Dictionary<string, string> groupItems in groupFolder2)
                                    {
                                        string nameGroupItem = String.Format("{0} {1}", groupItems["group2_INSRTSHT"], groupItems["item_OBOZN"]).Trim(' ');
                                        string guidGroupItem = groupItems["item_GUID"];
                                        guidClass = appData.getGuidTool(shortname, shortname, "Экземпляр");
                                        tagGroup = "item_";
                                        try
                                        {

                                            Dictionary<string, string> paramsItems = getParametersForGroup(groupItems, tagGroup, shortname, nameGroupItem, parametersGroup2);
                                            foreach (DinamicParam param in listDinamicParams.Where(x => x.guid == groupItems["item_GUID"] && x.fguid == groupItems["item_FGUID"]))
                                            {
                                                foreach (KeyValuePair<string, string> parameterItem in param.parameters)
                                                {
                                                    string name = parameterItem.Key;
                                                    string value = AppData.replaceChars(parameterItem.Value).Trim(' ');
                                                    paramsItems.Add(name, value);
                                                }
                                            }
                                            ReferenceObject obj = appTflex.createObjectCuttingTool(guidClass, paramsItems, folder2);
                                            updateProgresBarPre(countItems++, groupFolder2.Count(), countAllItems++, listLoadObjects.Count);
                                        }
                                        catch (Exception ex)
                                        {
                                            textError += String.Format("{0} > {1} > {2} > {3} ", nameGroupClass, nameGroupFolder, nameGroupFolder2, nameGroupItem);
                                            textError += String.Format("\n{0}", ex.Message);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    System.Diagnostics.Debugger.Break();
                                    textError += String.Format("{0} > {1} > {2}", nameGroupClass, nameGroupFolder, nameGroupFolder2);
                                    textError += String.Format("\n{0}", ex.Message);
                                }
                            }

                        }
                        catch (Exception ex)
                        {
                            System.Diagnostics.Debugger.Break();
                            textError += String.Format("{0} > {1}", nameGroupClass, nameGroupFolder);
                            textError += String.Format("\n{0}", ex.Message);
                        }
                    }



                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debugger.Break();
                    textError += String.Format("{0}", nameGroupClass);
                    textError += String.Format("\n{0}", ex.Message);
                }


            }
        }





    }



}
