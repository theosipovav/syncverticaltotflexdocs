﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyncVerticalToTFlexDocs.Models
{
    public class DinamicParam
    {
        public string guid { get; set; }
        public string fguid { get; set; }

        public Dictionary<string, string> parameters { get; set; }

        public DinamicParam(string guid, string fguid, List<Dictionary<string, string>> listParameters)
        {
            this.guid = guid;
            this.fguid = fguid;
            parameters = new Dictionary<string, string>();
            foreach (Dictionary<string, string> parameter in listParameters)
            {
                string name = parameter.FirstOrDefault(x => x.Key == "NAMESCREEN").Value;
                string value = parameter.FirstOrDefault(x => x.Key != "NAMESCREEN" && x.Value != "").Value;
                if (value == null)
                {
                    value = "";
                }
                this.parameters.Add(name, value);
            }
        }
    }
}
