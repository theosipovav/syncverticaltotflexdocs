﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using TFlex.DOCs.Model;
using TFlex.DOCs.Model.Classes;
using TFlex.DOCs.Model.Macros;
using TFlex.DOCs.Model.Macros.ObjectModel;
using TFlex.DOCs.Model.Parameters;
using TFlex.DOCs.Model.References;
using TFlex.DOCs.Model.References.Files;
using TFlex.DOCs.Model.Structure;

namespace SyncVerticalToTFlexDocs
{
    public class AppTflex
    {
        /// <summary>
        /// Соеденение с сервером приложений T-Flex DOCs
        /// </summary>
        ServerConnection serverConnection = null;

        public string textError { get; set; }

        /// <summary>
        /// Класс для работы с сервером приложения T-Flex DOCs
        /// </summary>
        /// <param name="username">Имя пользователя</param>
        /// <param name="password">Пароль</param>
        /// <param name="server">Сервер</param>
        public AppTflex(string username, string password, string server)
        {
            this.textError = "";

            try
            {
                // "Подключение к серверу
                serverConnection = ServerConnection.Open(username, password, server);
                if (!serverConnection.IsConnected)
                {
                    throw new Exception("Невозможно подключиться к серверу T-Flex DOCs");
                }

                // Подписываемся на событие потери соединения с сервером
                serverConnection.ConnectionLost += ServerGatewayOnConnectionLost;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Событие потери соединения с сервером
        /// </summary>
        private static void ServerGatewayOnConnectionLost(object sender, ConnectionLostEventArgs connectionLostEventArgs)
        {
            throw new Exception("Разрыв связи с серверо T-Flex DOCs");
        }



        public ReferenceObject findReferenceObject(ReferenceInfo referenceInfo, string name, string value)
        {
            Reference reference = referenceInfo.CreateReference();


            foreach (ClassObject classObject in referenceInfo.Classes.AllClasses)
            {
                foreach (ParameterGroup parameterGroups in classObject.ParameterGroups)
                {
                    ParameterInfo parameterInfo = parameterGroups.Parameters.FirstOrDefault(x => x.Name == name);
                    if (parameterInfo != null)
                    {
                        return reference.Find(parameterInfo, value).FirstOrDefault();
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Создать объект для справочника Средства технологического оснащения
        /// </summary>
        /// <param name="guidClass">GUID класса</param>
        /// <param name="parameters">Параметры создаваемого объекта</param>
        /// <param name="parent">Родитель создаваемого объекта</param>
        /// <returns></returns>
        public ReferenceObject createObjectCuttingTool(string guidClass, Dictionary<string, string> parameters, ReferenceObject parent = null)
        {
            string guidReference = "904fc7da-77df-4763-94b9-1ada11aafa4a";
            Dictionary<string, string> dinamicParameters = new Dictionary<string, string>();
            ReferenceObject newObject = null;
            try
            {
                // Поиск иили создание объекта
                ReferenceInfo referenceInfo = serverConnection.ReferenceCatalog.Find(new Guid(guidReference));
                Reference reference = referenceInfo.CreateReference();
                ClassObject classObject = referenceInfo.Classes.AllClasses.FirstOrDefault(x => x.Guid == new Guid(guidClass));
                ParameterGroupCollection parameterGroupsCollection = classObject.ParameterGroups;
                string guidVertical = parameters.FirstOrDefault(x => x.Key == "GUID Вертикаль").Value;
                newObject = findReferenceObject(referenceInfo, "GUID Вертикаль", guidVertical);
                if (newObject == null)
                {
                    newObject = reference.CreateReferenceObject(classObject);
                }
                else
                {
                    newObject.BeginChanges();
                }
                if (newObject == null)
                {
                    throw new Exception(String.Format("\nНе удалось создать объект типа [{0}]", guidClass));
                }

                // Работа с параметрами
                foreach (KeyValuePair<string, string> parameter in parameters.Where(x => x.Value != "" && x.Value != null))
                {
                    string currentParameterName = parameter.Key.TrimStart(' ').TrimEnd(' ');
                    string currentParameterValue = parameter.Value.TrimStart(' ').TrimEnd(' ');
                    bool isDinamic = true;
                    // Заполнение статических параметров
                    foreach (ParameterGroup parameterGroups in parameterGroupsCollection.Where(x => x.Type.Name == "Группа параметров" || x.Type.Name == "Справочник"))
                    {
                        try
                        {
                            ParameterInfo parameterInfo = parameterGroups.Parameters.FirstOrDefault(x => x.Name == currentParameterName && currentParameterValue != "");
                            if (parameterInfo != null)
                            {
                                newObject.ParameterValues[parameterInfo.Guid].Value = currentParameterValue;
                                isDinamic = false;
                                break;
                            }
                        }
                        catch (Exception ex)
                        {
                            System.Diagnostics.Debugger.Break();
                            this.textError += String.Format("\n Не удалось заполнить параметр: <{0}><{1}>\n{2}", currentParameterName, currentParameterValue, ex.Message);
                        }
                    }
                    if (isDinamic && !parameter.Key.Contains("class") && !parameter.Key.Contains("group") && !parameter.Key.Contains("item"))
                    {
                        dinamicParameters.Add(currentParameterName, currentParameterValue);
                    }
                }

                // Заполнение статических параметров
                foreach (ReferenceObject character in newObject.GetObjects(new Guid("7185f9e3-fb81-474b-8b93-0e06aeb15958")))
                {
                    try
                    {
                        character.Delete();
                    }
                    catch (Exception ex)
                    {
                        System.Diagnostics.Debugger.Break();
                    }
                }
                //
                // Обработка динамических параметров
                if (dinamicParameters.Count > 0)
                {
                    createDimanicParameters(newObject, dinamicParameters);
                }
                //
                // Обработка подключения к родительскому объекту
                if (parent != null)
                {
                    try
                    {
                        if (!parent.IsCheckedOut)
                        {
                            parent.BeginChanges();
                        }
                        newObject.SetParent(parent);
                        parent.EndChanges();
                    }
                    catch (Exception ex)
                    {
                        System.Diagnostics.Debugger.Break();
                    }
                }
                newObject.EndChanges();


            }
            catch (Exception ex)
            {
                System.Diagnostics.Debugger.Break();
                this.textError += String.Format("\n{0}", ex.Message);

                throw;
            }


            return newObject;
        }



        public void createDimanicParameters(ReferenceObject objPerent, Dictionary<string, string> dinamicParameters)
        {
            Guid guidObjectLists = new Guid("7185f9e3-fb81-474b-8b93-0e06aeb15958");
            Guid guidObjectListsType = new Guid("27411684-9d47-404f-afc9-97d95689561a");
            foreach (KeyValuePair<string, string> parameter in dinamicParameters)
            {

                ReferenceObject obj = objPerent.CreateListObject(guidObjectLists, guidObjectListsType);
                ParameterGroup parameterGroup = obj.Class.ParameterGroups.Find(new Guid("7185f9e3-fb81-474b-8b93-0e06aeb15958"));
                ParameterInfo parameterInfo;
                // Наименование
                parameterInfo = parameterGroup.Parameters.First(x => x.Guid == new Guid("2767522b-a4ff-4778-a0b6-89b812aae086"));
                obj.ParameterValues[parameterInfo.Guid].Value = parameter.Key;
                // Значение
                parameterInfo = parameterGroup.Parameters.First(x => x.Guid == new Guid("88c30451-1326-4490-8f2f-7b3cf5afe572"));
                obj.ParameterValues[parameterInfo.Guid].Value = parameter.Value;
                obj.EndChanges();
            }
        }


    }
}
