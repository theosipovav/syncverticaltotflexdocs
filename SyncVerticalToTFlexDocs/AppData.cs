﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyncVerticalToTFlexDocs
{
    class AppData
    {
        /// <summary>
        /// 
        /// </summary>
        List<string[]> listGuidTool { get; set; }

        /// <summary>
        /// 
        /// </summary>
        List<string[]> listReplaceNameParameterVertical { get; set; }

        /// <summary>
        /// 
        /// </summary>
        List<string[]> listReplaceNameSQLParam { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public AppData()
        {
            listGuidTool = AppData.loadGuidTool();
            listReplaceNameParameterVertical = AppData.loadReplaceNameParameterVertical();
            listReplaceNameSQLParam = AppData.loadReplaceNameSQLParam();
        }

        /// <summary>
        /// Получить GUID папки или экземплята, взависимости от режущего инстурмента
        /// </summary>
        /// <param name="nameClass">Наименование типа</param>
        /// <param name="nameClassMain">Наименование типа, от которого он порождён</param>
        /// <param name="type">Тип объекта: "Группа" или "Экземпляр"</param>
        /// <returns></returns>
        public string getGuidTool(string nameClass, string nameClassMain, string typeTflexObject)
        {
            string guid = "";
            guid = listGuidTool.FirstOrDefault(x => x[0] == nameClassMain && x[1] == nameClass && x[2] == typeTflexObject)[3];
            if (guid == "" || guid == null)
            {
                throw new Exception(String.Format("Не найден GUID папки или экземплята, взависимости от режущего инстурмента: {0}, {1}, {2}", nameClass, nameClassMain, typeTflexObject));
            }
            return guid;

        }


        /// <summary>
        /// Заменить наименование параметров
        /// </summary>
        /// <param name="name">Наименование параметра для замены</param>
        /// <returns></returns>
        public string replaceNameParameterVertical(string name, string nameClassMain)
        {
            if (listReplaceNameParameterVertical.Count(x => x[0] == nameClassMain && x[1] == name) > 0)
            {
                return listReplaceNameParameterVertical.First(x => x[0] == nameClassMain && x[1] == name)[2];
            }
            return name;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public string replaceNameSQLParam(string name, string nameClassMain)
        {

            if (listReplaceNameSQLParam.Count(x => x[0] == nameClassMain && x[1] == name) > 0)
            {
                return listReplaceNameSQLParam.First(x => x[0] == nameClassMain && x[1] == name)[2];
            }
            return name;
        }

        /// <summary>
        /// Заменить символы
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        static public string replaceChars(string str)
        {
            str = str.Replace('‡', 'Ø');
            return str;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        static private List<string[]> loadGuidTool()
        {
            List<string[]> list = new List<string[]>();
            // Заполнение GUID папки или экземплята, взависимости от режущего инстурмента 
            // Режущий инструмент
            list.Add(new string[4] { "РИ", "РИ", "Группа", "059438f6-6bd6-4880-bb84-d4105bf8046f" });
            list.Add(new string[4] { "РИ", "РИ", "Экземпляр", "0cc6449c-149c-45a0-8dd9-675d152b5340" });
            // Вспомогательный инструмент
            list.Add(new string[4] { "ВИ", "ВИ", "Группа", "3544d24d-349f-4dd1-b881-e9f31edac003" });
            list.Add(new string[4] { "ВИ", "ВИ", "Экземпляр", "429188c2-5881-43da-9ff7-faaef2f92c0e" });
            // Режущая часть - пластинки
            list.Add(new string[4] { "РЧП", "РЧП", "Группа", "12ce43db-916c-4f93-a296-4c0ee36cbea1" });
            list.Add(new string[4] { "РЧП", "РЧП", "Экземпляр", "4cacadcb-150b-470c-b391-2a1eb0429b25" });
            // Измерительный инструмент - ИИ
            list.Add(new string[4] { "ИИ", "ИИ", "Группа", "f6d67533-57b3-4c12-b31e-b0853590a429" });
            list.Add(new string[4] { "ИИ", "ИИ", "Экземпляр", "3472437e-18db-4dc6-adbf-b2bba6c3048c" });
            // Измерительные приборы = ИП
            list.Add(new string[4] { "ИП", "Д0.ХХХ.ХХХХХ", "Группа", "d5d459ad-8fdd-46b1-bb14-322919efd971" });
            list.Add(new string[4] { "ИП", "Д0.ХХХ.ХХХХХ", "Экземпляр", "b3e22d57-90d3-44a4-83c0-9f52816a0004" });
            list.Add(new string[4] { "ИП", "Д4.ХХХ.ХХХХХ", "Группа", "368d2397-377c-481b-ba1a-5c0fe1935629" });
            list.Add(new string[4] { "ИП", "Д4.ХХХ.ХХХХХ", "Экземпляр", "09592c0b-0dab-4059-800e-19c7430bca12" });
            list.Add(new string[4] { "ИП", "Д6.ХХХ.ХХХХХ", "Группа", "69c1b796-bb05-45af-8fd6-7446e161db73" });
            list.Add(new string[4] { "ИП", "Д6.ХХХ.ХХХХХ", "Экземпляр", "f991b986-c33d-4bc2-880c-85e130f97793" });
            list.Add(new string[4] { "ИП", "Измерение", "Группа", "a1c86457-a45c-4026-8df5-cef4adcb6519" });
            list.Add(new string[4] { "ИП", "Измерение", "Экземпляр", "c9e0154c-d2d8-4d17-907b-1e13e345a12c" });
            list.Add(new string[4] { "ИП", "Испытания", "Группа", "66e8ad37-cc8e-4477-9f55-8ec0d1fefba8" });
            list.Add(new string[4] { "ИП", "Испытания", "Экземпляр", "f729db32-b692-49c4-9c24-50b94a84d8ac" });
            list.Add(new string[4] { "ИП", "Контроль механических свойств", "Группа", "84a1a870-6e0f-45d0-817c-a4aaa51e5652" });
            list.Add(new string[4] { "ИП", "Контроль механических свойств", "Экземпляр", "4826f943-83f9-4e4d-ae8b-bd8886139fd7" });
            list.Add(new string[4] { "ИП", "Контроль окружающей обстановки", "Группа", "29ff2436-8ca1-415d-85f6-f79f8a765241" });
            list.Add(new string[4] { "ИП", "Контроль окружающей обстановки", "Экземпляр", "54224a67-5b46-41bd-a5d3-8c58690131fa" });
            list.Add(new string[4] { "ИП", "Контроль хим.состава и физ.св.", "Группа", "d7758804-d34d-474a-ac87-6daf8a2d5849" });
            list.Add(new string[4] { "ИП", "Контроль хим.состава и физ.св.", "Экземпляр", "6ce293b2-93dc-42c1-a5d1-7adf1de40d4e" });
            list.Add(new string[4] { "ИП", "Лабораторные и клинич.условия", "Группа", "8ea8788d-3469-46fa-be38-e8b066604b6a" });
            list.Add(new string[4] { "ИП", "Лабораторные и клинич.условия", "Экземпляр", "e22cab55-7e47-4858-bdd3-bb5de75af5bf" });
            list.Add(new string[4] { "ИП", "Неразрушающий контроль", "Группа", "807fd0af-32b8-4e7d-a247-434f32d691a0" });
            list.Add(new string[4] { "ИП", "Неразрушающий контроль", "Экземпляр", "f8de1966-b077-473c-aba6-e4649b386907" });
            list.Add(new string[4] { "ИП", "Прочее", "Группа", "98d5ed7a-8adb-4421-bcc7-4042fd8c9536" });
            list.Add(new string[4] { "ИП", "Прочее", "Экземпляр", "cf33fa9c-5efa-481c-a565-ba1d925f37fd" });
            // Тара
            list.Add(new string[4] { "Тара", "Тара", "Группа", "e0575116-2406-4d71-9815-a9c4fc5949ec" });
            list.Add(new string[4] { "Тара", "Тара", "Экземпляр", "2b1ca8de-73e2-4e82-b6c4-dfd16086d570" });
            // ИП
            list.Add(new string[4] { "ИП", "ИП", "Группа", "c8fc4e95-dee1-4880-975b-ed02ddb8973b" });
            list.Add(new string[4] { "ИП", "ИП", "Экземпляр", "0f706892-9e09-4b30-a3f0-3407ea9a7713" });
            // Штамповочная оснастка - ШО
            list.Add(new string[4] { "ШО", "ШО", "Группа", "cbefba93-9d3c-47af-9aff-adcdb619ff11" });
            list.Add(new string[4] { "ШО", "ШО", "Экземпляр", "c2b9b261-4718-4a57-ac37-2fc83d839ee8" });
            // Штамповочная оснастка - ШИ
            list.Add(new string[4] { "ШИ", "ШИ", "Группа", "5bd80844-1cf7-4f3e-a589-4ede08340fa5" });
            list.Add(new string[4] { "ШИ", "ШИ", "Экземпляр", "742eecfe-99e8-446b-aa2d-89d3e9a0d30c" });
            // Станочные приспособления - СП
            list.Add(new string[4] { "СП", "СП", "Группа", "1e1b799e-5232-4f9b-bbe9-f77110e29aa7" });
            list.Add(new string[4] { "СП", "СП", "Экземпляр", "1a90e080-5e52-4280-a116-0af7791001d9" });
            // Оснастка лакокраски - ОЛ
            list.Add(new string[4] { "ОЛ", "ОЛ", "Группа", "61f5a3f8-db3f-4ccc-9542-b4ba550d93d3" });
            list.Add(new string[4] { "ОЛ", "ОЛ", "Экземпляр", "065df256-ba7b-47d9-9023-66b1bb72dde5" });
            // Оснастка термообработки - ОТ
            list.Add(new string[4] { "ОТ", "ОТ", "Группа", "d3aed08a-139f-45e1-9ef1-8bda59c8f785" });
            list.Add(new string[4] { "ОТ", "ОТ", "Экземпляр", "fc4e09e2-49ea-4aad-9b0c-66a6065bd52b" });
            // Приборы - Приборы
            list.Add(new string[4] { "Приборы", "Приборы", "Группа", "3ad92f3e-c87b-48d2-8159-647328e7fb26" });
            list.Add(new string[4] { "Приборы", "Приборы", "Экземпляр", "b36bd42a-ac9c-48c7-8fde-ff6958b1704e" });
            // Сборочная оснастка - СБО
            list.Add(new string[4] { "СБО", "СБО", "Группа", "a72bc929-87cc-46a2-9c3e-94d68947c241" });
            list.Add(new string[4] { "СБО", "СБО", "Экземпляр", "17c9227b-e1a9-44a2-8673-b37349ce7df4" });
            // Сварочная оснастка - СВО
            list.Add(new string[4] { "СВО", "СВО", "Группа", "1e3cd5c9-a5e9-4741-b904-3d8f2d6e250e" });
            list.Add(new string[4] { "СВО", "СВО", "Экземпляр", "278b967c-01fd-4b33-af4e-0a48a4da6ec4" });
            // 
            // Слесарный инструмент - СИ
            list.Add(new string[4] { "СИ", "СИ", "Группа", "c92fd2bf-4b22-4e61-a852-b81b4744e92a" });
            list.Add(new string[4] { "СИ", "СИ", "Экземпляр", "4b9e5cf1-b64a-4806-befd-74f462b7d59d" });
            return list;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        static private List<string[]> loadReplaceNameParameterVertical()
        {
            List<string[]> list = new List<string[]>();
            // Режущий инструмент
            list.Add(new string[3] { "РИ", "B||Ширина фрезы, державки резца [мм], B", "Ширина фрезы, державки резца, B [мм]" });
            list.Add(new string[3] { "РИ", "D (мм)||Диаметр осевого инструмента [мм]", "Диаметр осевого инструмента, D [мм]" });
            list.Add(new string[3] { "РИ", "D max растачивания, мм", "D max растачивания [мм]" });
            list.Add(new string[3] { "РИ", "D min растачивания, мм", "D min растачивания [мм]" });
            list.Add(new string[3] { "РИ", "d min||Мин. диаметр обр. отверстия", "Мин. диаметр обр. отверстия" });
            list.Add(new string[3] { "РИ", "D державки||Диаметр держ./оправки/хвостовика", "Диаметр держ./оправки/хвостовика, D" });
            list.Add(new string[3] { "РИ", "D круга||Диаметр круга, мм", "Диаметр круга, D [мм]" });
            list.Add(new string[3] { "РИ", "D фрезы||Диаметр фрезы", "Диаметр фрезы, D" });
            list.Add(new string[3] { "РИ", "d(мм)||Диаметр отверстия", "Диаметр отверстия, d [мм]" });
            list.Add(new string[3] { "РИ", "D(мм)||Диаметр резьбы, D [мм]", "Диаметр резьбы, D [мм]" });
            list.Add(new string[3] { "РИ", "D||Диаметр головки", "Диаметр головки, D" });
            list.Add(new string[3] { "РИ", "d||Посадочный диаметр, d", "Посадочный диаметр, d" });
            list.Add(new string[3] { "РИ", "D0||Делительный диам., D0", "Делительный диам., D0" });
            list.Add(new string[3] { "РИ", "d0||Диаметр делительной окружности, d0", "d0" });
            list.Add(new string[3] { "РИ", "D1 min растачивания, мм", "D1 min растачивания, [мм]" });
            list.Add(new string[3] { "РИ", "D1||Нижнее откл. D1 [мм]", "Нижнее откл., D1 [мм]" });
            list.Add(new string[3] { "РИ", "Dao||Диам. вершин зубьев, Dao", "Диам. вершин зубьев, Dao" });
            list.Add(new string[3] { "РИ", "Dотв.||Макс. D центрового отверстия по ГОСТ 14034-74", "Max D центрового отверстия по ГОСТ 14034-74" });
            list.Add(new string[3] { "РИ", "dотв||Допуск диаметра отв.", "Допуск диаметра отв., dотв" });
            list.Add(new string[3] { "РИ", "dхв., мм||Диаметр хвостовика, dхв. [мм]", "Диаметр хвостовика, dхв. [мм]" });
            list.Add(new string[3] { "РИ", "f, мм/об||Подача на оборот (f, мм/об)", "Подача на оборот, f [мм/об]" });
            list.Add(new string[3] { "РИ", "fz – подача на зуб, мм/зуб;", "Подача на зуб, fz [мм/зуб]" });
            list.Add(new string[3] { "РИ", "Fz, мм/зуб||Подача на зуб, Fz [мм/зуб]", "Подача на зуб, fz [мм/зуб]" });
            list.Add(new string[3] { "РИ", "H (мм)||Ширина круга, мм", "Ширина круга, H [мм]" });
            list.Add(new string[3] { "РИ", "H винтовой канавки||Ширина винтовой канавки сверла [мм], B", "Ширина винтовой канавки сверла, B [мм]" });
            list.Add(new string[3] { "РИ", "H зубьев||Количество зубьев на сегмент", "Количество зубьев на сегмент, H" });
            list.Add(new string[3] { "РИ", "H||Высота державки [мм], H", "Высота державки, H [мм]" });
            list.Add(new string[3] { "РИ", "h||Фаска c", "Фаска" });
            list.Add(new string[3] { "РИ", "H||Ширина алмазоносного слоя", "Ширина алмазоносного слоя, H" });
            list.Add(new string[3] { "РИ", "H||Ширина ленточки сверла, [мм], F", "Ширина ленточки сверла, F [мм]" });
            list.Add(new string[3] { "РИ", "h1 max||Макс. высота головки, h1 max", "Max высота головки, h1 max" });
            list.Add(new string[3] { "РИ", "h1 min||Мин. высота головки, h1 min.", "Min высота головки, h1 min" });
            list.Add(new string[3] { "РИ", "I", "l (реж.)" });
            list.Add(new string[3] { "РИ", "K||Размер сердцевины сверла, [мм], K", "Размер сердцевины сверла, K [мм]" });
            list.Add(new string[3] { "РИ", "l", "l (реж.)" });
            list.Add(new string[3] { "РИ", "L номин., мм", "L номин., [мм]" });
            list.Add(new string[3] { "РИ", "L||Длина общая L", "Длина общая, L" });
            list.Add(new string[3] { "РИ", "L||Длина режущей кромки", "Длина режущей кромки, L" });
            list.Add(new string[3] { "РИ", "L||Нижнее откл.длины L", "Нижнее откл. длины, L" });
            list.Add(new string[3] { "РИ", "l1получист.", "l1 получист." });
            list.Add(new string[3] { "РИ", "l1чист.", "l1 чист." });
            list.Add(new string[3] { "РИ", "Lо", "L0" });
            list.Add(new string[3] { "РИ", "lо", "l0" });
            list.Add(new string[3] { "РИ", "Lобщ.||Длина общая L (короткая серия)", "Длина общая L (короткая серия)" });
            list.Add(new string[3] { "РИ", "Lр, мм||Длина режущей части, Lр [мм]", "Длина режущей части, Lр [мм]" });
            list.Add(new string[3] { "РИ", "Lр,мм||Рекомендуемая глубина сверления (Lр,мм)", "Рекомендуемая глубина сверления, Lр [мм]" });
            list.Add(new string[3] { "РИ", "Lраст, мм||Длина растачивания (Lраст, мм)", "Длина растачивания, Lраст [мм]" });
            list.Add(new string[3] { "РИ", "m(to)||Окружной модуль m(to)", "Окружной модуль m(to)" });
            list.Add(new string[3] { "РИ", "m||Модуль m", "Модуль m" });
            list.Add(new string[3] { "РИ", "n – скорость вращения шпинделя, об/мин", "Скорость вращения шпинделя, n [об/мин]" });
            list.Add(new string[3] { "РИ", "n, об/мин||Скорость вращения шпинделя, n [об/мин]", "Скорость вращения шпинделя, n [об/мин]" });
            list.Add(new string[3] { "РИ", "№||Номер структуры, №", "Номер структуры, №" });
            list.Add(new string[3] { "РИ", "P||Макс. шаг, P", "Max шаг, P" });
            list.Add(new string[3] { "РИ", "P||Мин. шаг, P", "Min шаг, P" });
            list.Add(new string[3] { "РИ", "P||Шаг, P", "Шаг, P" });
            list.Add(new string[3] { "РИ", "Pn||Шаг захода, Pn", "Шаг захода, Pn" });
            list.Add(new string[3] { "РИ", "Pz||Ход зуба делительный, Pz", "Ход зуба делительный, Pz" });
            list.Add(new string[3] { "РИ", "R1||Радиус при вершине [мм]", "Радиус при вершине, R1 [мм]" });
            list.Add(new string[3] { "РИ", "Rzak фрезы||Радиус закругления фрезы [мм]", "Радиус закругления фрезы [мм]" });
            list.Add(new string[3] { "РИ", "Rzak||Радиус закругления [мм], R", "Радиус закругления, R [мм]" });
            list.Add(new string[3] { "РИ", "Rзакр.||Нижн.отклонение радиуса закругления [мм]", "Нижнее откл. радиуса закругления, R [мм]" });
            list.Add(new string[3] { "РИ", "S м/с||Скорость, S м/с", "Скорость, S [м/с]" });
            list.Add(new string[3] { "РИ", "S||Размер квадрата, S", "Размер квадрата, S" });
            list.Add(new string[3] { "РИ", "S||Размер под ключ, S", "Размер под ключ, S" });
            list.Add(new string[3] { "РИ", "S||Шаг стружкоразд. канавок, S [мм]", "Шаг стружкоразд. канавок, S [мм]" });
            list.Add(new string[3] { "РИ", "S0", "S0" });
            list.Add(new string[3] { "РИ", "Smax||Скорость рабочая предельная, Smax (м/с)", "Скорость рабочая предельная, Smax [м/с]" });
            list.Add(new string[3] { "РИ", "t", "t" });
            list.Add(new string[3] { "РИ", "t||Шаг, t", "Шаг, t" });
            list.Add(new string[3] { "РИ", "V, м/мин||Скорость резания, V [м/мин]", "Скорость резания, V [м/мин]" });
            list.Add(new string[3] { "РИ", "Z||Число зубьев/рядов зубьев", "Число зубьев/рядов зубьев, Z" });
            list.Add(new string[3] { "РИ", "ZamDop||ZamDop - Допустимые фрезы Д1.223.", "Допустимые фрезы Д1.223." });
            list.Add(new string[3] { "РИ", "ZamRek||ZamRek - Рекомендуемые фрезы Д1.223", "Рекомендуемые фрезы Д1.223" });
            list.Add(new string[3] { "РИ", "Всп. угол в плане||Вспомогательный угол в плане [град] фи1", "Вспомогательный угол в плане [град] фи1" });
            list.Add(new string[3] { "РИ", "Дв. угол в плане, [град] Фи2", "Дв. угол в плане, [град] Фи3" });
            list.Add(new string[3] { "РИ", "Задний угол||Задний угол [град]", "Задний угол [град]" });
            list.Add(new string[3] { "РИ", "Кол-во отв.||Количество посадочных отверстий", "Количество посадочных отверстий" });
            list.Add(new string[3] { "РИ", "Кол-во сегмент||Количество сегментов", "Количество сегментов" });
            list.Add(new string[3] { "РИ", "Отклонение L||Нижнее откл. длины реж. части [мм]", "Нижнее откл. длины реж. части, L [мм]" });
            list.Add(new string[3] { "РИ", "Угол в плане||Угол в плане [град], фи", "Угол в плане [град], фи" });
            list.Add(new string[3] { "РИ", "Угол наклона||Угол наклона зубьев/винт.канавки [град]", "Угол наклона зубьев/винт.канавки [град]" });
            list.Add(new string[3] { "РИ", "Угол||Передний угол [град]", "Передний угол [град]" });
            list.Add(new string[3] { "РИ", "Инструмент фрез", "Инструмент фрез." });
            // Вспомогательный инструмент
            list.Add(new string[3] { "ВИ", "Наименование подгруппы ВИ", "Подгруппа ВИ" });
            list.Add(new string[3] { "ВИ", "Do", "D0" });
            // Режущая часть (пластинки)
            list.Add(new string[3] { "РЧП", "D||Диаметр растачивания (D, мм)", "Диаметр растачивания, D [мм]" });
            list.Add(new string[3] { "РЧП", "Dmin, мм||min растачиваемое отв. (Dmin, мм)", "Min растачиваемое отв., Dmin [мм]" });
            list.Add(new string[3] { "РЧП", "dхв, мм.||Диаметр хвостовика (dхв, мм.)", "Диаметр хвостовика, dхв. [мм]" });
            list.Add(new string[3] { "РЧП", "Firma", "Фирма" });
            list.Add(new string[3] { "РЧП", "fz, мм/зуб.||Подача на зуб (fz, мм/зуб.)", "Подача на зуб, fz [мм/зуб]" });
            list.Add(new string[3] { "РЧП", "fz||fz – подача на зуб, мм/зуб (текстовый);", "Подача на зуб, fz [мм/зуб]" });
            list.Add(new string[3] { "РЧП", "Lр_max, мм||max растачиваемая длина (Lр_max, мм)", "Max растачиваемая длина, Lр_max [мм]" });
            list.Add(new string[3] { "РЧП", "n, об/мин.||Скорость вращения шпинделя (n, об/мин.)", "Скорость вращения шпинделя, n [об/мин]" });
            list.Add(new string[3] { "РЧП", "n||n – скорость вращения шпинделя, об/мин.(текстовый).", "Скорость вращения шпинделя, n [об/мин]" });
            list.Add(new string[3] { "РЧП", "r||Радиус (r, мм)", "Радиус при вершине, R [мм]" });
            list.Add(new string[3] { "РЧП", "s, мм/об.||Подача (s, мм/об)", "Подача, s [мм/об]" });
            list.Add(new string[3] { "РЧП", "S||S – подача, мм/мин (текстовый);", "Подача, S [мм/мин]" });
            list.Add(new string[3] { "РЧП", "Tmax, мм||Глубина канавки max (Tmax, мм)", "Глубина канавки max, Tmax [мм]" });
            list.Add(new string[3] { "РЧП", "V, м/мин.||Скорость резания (V, м/мин)", "Скорость резания, V [м/мин]" });
            list.Add(new string[3] { "РЧП", "z||Число зубьев (z)", "Число зубьев, z" });
            list.Add(new string[3] { "РЧП", "ар, мм.||Глубина резания (ар, мм)", "Глубина резания, ар [мм]" });
            list.Add(new string[3] { "РЧП", "В, мм||Ширина пластины (В, мм)", "Ширина пластины В, [мм]" });
            list.Add(new string[3] { "РЧП", "Радиус при вершине (R, мм)", "Радиус при вершине, R [мм]" });
            // - ИИ
            list.Add(new string[3] { "ШО", "Оснастка", "Обозначение" });

            return list;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        static private List<string[]> loadReplaceNameSQLParam()
        {
            List<string[]> load = new List<string[]>();
            // Режущий инструмент
            load.Add(new string[3] { "РИ", "class_NAME", "Вид инструмента" });
            load.Add(new string[3] { "РИ", "class_GUID", "GUID Вертикаль" });
            load.Add(new string[3] { "РИ", "group_GROUPRI", "Наименование типа РИ" });
            load.Add(new string[3] { "РИ", "group_TIPRI", "Тип РИ" });
            load.Add(new string[3] { "РИ", "group_PEREXOD", "Метод обработки" });
            load.Add(new string[3] { "РИ", "group_GUID", "GUID Вертикаль" });
            load.Add(new string[3] { "РИ", "group2_GOST", "ГОСТ или ТУ" });
            load.Add(new string[3] { "РИ", "group2_PRIM", "Описание" });
            load.Add(new string[3] { "РИ", "group2_NAME_RI", "Режущий инструмент" });
            load.Add(new string[3] { "РИ", "group2_COMMENTS", "Наименование реж.инструм." });
            load.Add(new string[3] { "РИ", "group2_GUID", "GUID Вертикаль" });
            load.Add(new string[3] { "РИ", "item_NAME", "Наименование" });
            load.Add(new string[3] { "РИ", "item_OBOZN", "Обозначение" });
            load.Add(new string[3] { "РИ", "item_COD_START", "Код КСУ" });
            load.Add(new string[3] { "РИ", "item_MATRLOCATION", "Марка материала РИ" });
            load.Add(new string[3] { "РИ", "item_GUID", "GUID Вертикаль" });
            // Вспомогательный инструмент
            load.Add(new string[3] { "ВИ", "class_NAMETIPOPER", "Вид" });
            load.Add(new string[3] { "ВИ", "class_GUID", "GUID Вертикаль" });
            load.Add(new string[3] { "ВИ", "group_A", "Группа вспом. инструмента" });
            load.Add(new string[3] { "ВИ", "group_B", "Группа вспом. инструмента" });
            load.Add(new string[3] { "ВИ", "group_C", "Группа вспом. инструмента" });
            load.Add(new string[3] { "ВИ", "group_GUID", "GUID Вертикаль" });
            load.Add(new string[3] { "ВИ", "group2_COMMENTS", "Подгруппа ВИ" });
            load.Add(new string[3] { "ВИ", "group2_NAME_VI", "Вспомогательный инстр." });
            load.Add(new string[3] { "ВИ", "group2_GOST", "ГОСТ или ТУ" });
            load.Add(new string[3] { "ВИ", "group2_PRIMVI", "Описание" });
            load.Add(new string[3] { "ВИ", "group2_GUID", "GUID Вертикаль" });
            load.Add(new string[3] { "ВИ", "item_OBOZN", "Обозначение" });
            load.Add(new string[3] { "ВИ", "item_NAME", "Наименование" });
            load.Add(new string[3] { "ВИ", "item_COD_START", "Код КСУ" });
            load.Add(new string[3] { "ВИ", "item_GUID", "GUID Вертикаль" });
            // Режущая часть (пластинки)
            load.Add(new string[3] { "РЧП", "class_NAME", "Вид" });
            load.Add(new string[3] { "РЧП", "class_GUID", "GUID Вертикаль" });
            load.Add(new string[3] { "РЧП", "group_NAME", "Группа" });
            load.Add(new string[3] { "РЧП", "group_GUID", "GUID Вертикаль" });
            load.Add(new string[3] { "РЧП", "group2_NAME", "Наименование пластинки" });
            load.Add(new string[3] { "РЧП", "group2_COMMENTS", "Рекомендуемое назначение" });
            load.Add(new string[3] { "РЧП", "group2_FORMPLATE", "Код формы пластины" });
            load.Add(new string[3] { "РЧП", "group2_OBOZN_ISO", "Обозначение группы" });
            load.Add(new string[3] { "РЧП", "group2_GUID", "GUID Вертикаль" });
            load.Add(new string[3] { "РЧП", "item_COATING", "Наличие покрытия" });
            load.Add(new string[3] { "РЧП", "item_COD_START", "Код КСУ" });
            load.Add(new string[3] { "РЧП", "item_D", "D" });
            load.Add(new string[3] { "РЧП", "item_D1", "D1" });
            load.Add(new string[3] { "РЧП", "item_L", "L" });
            load.Add(new string[3] { "РЧП", "item_MATRI", "Материал РИ" });
            load.Add(new string[3] { "РЧП", "item_OBOZN", "Обозначение" });
            load.Add(new string[3] { "РЧП", "item_R", "Радиус" });
            load.Add(new string[3] { "РЧП", "item_S", "Толщина пластины" });
            load.Add(new string[3] { "РЧП", "item_GUID", "GUID Вертикаль" });
            // Измерительный инструмент - ИИ
            load.Add(new string[3] { "ИИ", "class_GROUPII", "Вид" });
            load.Add(new string[3] { "ИИ", "class_GUID", "GUID Вертикаль" });
            load.Add(new string[3] { "ИИ", "group_COMMENTS", "Наименование изм.инструм." });
            load.Add(new string[3] { "ИИ", "group_NAME_II", "Измерительный инструмент" });
            load.Add(new string[3] { "ИИ", "group_GOST", "ГОСТ или ТУ" });
            load.Add(new string[3] { "ИИ", "group_KODGRII", "Код группы ИИ" });
            load.Add(new string[3] { "ИИ", "group_PRIM", "Описание" });
            load.Add(new string[3] { "ИИ", "group_KODRAST", "Код расчета времени" });
            load.Add(new string[3] { "ИИ", "group_GUID", "GUID Вертикаль" });
            load.Add(new string[3] { "ИИ", "item_NAME", "Наименование" });
            load.Add(new string[3] { "ИИ", "item_OBOZN", "Обозначение" });
            load.Add(new string[3] { "ИИ", "item_QUALITET", "Квалитет" });
            load.Add(new string[3] { "ИИ", "item_COD_START", "Код КСУ" });
            load.Add(new string[3] { "ИИ", "item_GUID", "GUID Вертикаль" });
            // Тара
            load.Add(new string[3] { "Тара", "class_NAME", "Наименование группы" });
            load.Add(new string[3] { "Тара", "class_GUID", "GUID Вертикаль" });
            load.Add(new string[3] { "Тара", "item_B_TARA", "Ширина В(мм)" });
            load.Add(new string[3] { "Тара", "item_COD_START", "Код КСУ" });
            load.Add(new string[3] { "Тара", "item_GOST", "ГОСТ или ТУ" });
            load.Add(new string[3] { "Тара", "item_H_TARA", "Высота Н(мм)" });
            load.Add(new string[3] { "Тара", "item_NAME", "Наименование" });
            load.Add(new string[3] { "Тара", "item_OBOZN", "Обозначение" });
            load.Add(new string[3] { "Тара", "item_PRIM", "Характеристика тары" });
            load.Add(new string[3] { "Тара", "item_GUID", "GUID Вертикаль" });
            // Измерительные приборы - ИП
            load.Add(new string[3] { "ИП", "class_A", "Вид" });
            load.Add(new string[3] { "ИП", "class_B", "Код вида" });
            load.Add(new string[3] { "ИП", "class_GUID", "GUID Вертикаль" });
            load.Add(new string[3] { "ИП", "group_NAMETIPIZ", "Метод контроля" });
            load.Add(new string[3] { "ИП", "group_GUID", "GUID Вертикаль" });
            load.Add(new string[3] { "ИП", "item_COD_START", "Код КСУ" });
            load.Add(new string[3] { "ИП", "item_GOST", "ГОСТ или ТУ" });
            load.Add(new string[3] { "ИП", "item_IZMSVR", "Марка" });
            load.Add(new string[3] { "ИП", "item_NAMEIZM", "Наименование" });
            load.Add(new string[3] { "ИП", "item_VALIZM", "Значение" });
            load.Add(new string[3] { "ИП", "item_XARIZM", "Характеристика" });
            load.Add(new string[3] { "ИП", "item_GUID", "GUID Вертикаль" });
            // Штамповочная оснастка - ШО
            load.Add(new string[3] { "ШО", "class_A", "Вид" });
            load.Add(new string[3] { "ШО", "class_B", "Код типа" });
            load.Add(new string[3] { "ШО", "class_GUID", "GUID Вертикаль" });
            load.Add(new string[3] { "ШО", "group_KODGR", "Код группы" });
            load.Add(new string[3] { "ШО", "group_KODTIP", "Код типа" });
            load.Add(new string[3] { "ШО", "group_NAMEGROSN", "Группа оснастки" });
            load.Add(new string[3] { "ШО", "group_GUID", "GUID Вертикаль" });
            load.Add(new string[3] { "ШО", "item_COD_START", "Код КСУ" });
            load.Add(new string[3] { "ШО", "item_KODGROSN", "Группа оснастки" });
            load.Add(new string[3] { "ШО", "item_NAMEOSN", "Наименование" });
            load.Add(new string[3] { "ШО", "item_OSNAS", "Обозначение" });
            load.Add(new string[3] { "ШО", "item_GUID", "GUID Вертикаль" });
            // Штамповочный инструмент - ШИ
            load.Add(new string[3] { "ШИ", "class_A", "Вид" });
            load.Add(new string[3] { "ШИ", "class_B", "Код типа" });
            load.Add(new string[3] { "ШИ", "class_GUID", "GUID Вертикаль" });
            load.Add(new string[3] { "ШИ", "group_NAMEGRINS", "Группа инструмента" });
            load.Add(new string[3] { "ШИ", "group_KODGRINS", "Код группы" });
            load.Add(new string[3] { "ШИ", "group_GUID", "GUID Вертикаль" });
            load.Add(new string[3] { "ШИ", "item_NAMEINSTR", "Наименование" });
            load.Add(new string[3] { "ШИ", "item_INSRTSHT", "Обозначение" });
            load.Add(new string[3] { "ШИ", "item_KODGR", "Код подгруппы" });
            load.Add(new string[3] { "ШИ", "item_COD_START", "Код КСУ" });
            load.Add(new string[3] { "ШИ", "item_GUID", "GUID Вертикаль" });
            // Станочные приспособления - СП
            load.Add(new string[3] { "СП", "class_VID_PRISP", "Вид" });
            load.Add(new string[3] { "СП", "class_GUID", "GUID Вертикаль" });
            load.Add(new string[3] { "СП", "group_A", "Тип приспособления" });
            load.Add(new string[3] { "СП", "group_GUID", "GUID Вертикаль" });
            load.Add(new string[3] { "СП", "group2_COMMENTS", "Наименование приспособления" });
            load.Add(new string[3] { "СП", "group2_NAME_PRISP", "Приспособление" });
            load.Add(new string[3] { "СП", "group2_GOST", "ГОСТ или ТУ" });
            load.Add(new string[3] { "СП", "group2_PRIM", "Описание" });
            load.Add(new string[3] { "СП", "group2_GUID", "GUID Вертикаль" });
            load.Add(new string[3] { "СП", "item_OBOZN", "Обозначение" });
            load.Add(new string[3] { "СП", "item_COD_START", "Код КСУ" });
            load.Add(new string[3] { "СП", "item_GUID", "GUID Вертикаль" });
            // Станочные приспособления - ОЛ
            load.Add(new string[3] { "ОЛ", "class_VID", "Вид" });
            load.Add(new string[3] { "ОЛ", "class_GUID", "GUID Вертикаль" });
            load.Add(new string[3] { "ОЛ", "item_NAMEINSR", "Наименование" });
            load.Add(new string[3] { "ОЛ", "item_OBOZINST", "Обозначение" });
            load.Add(new string[3] { "ОЛ", "item_COMMENTS", "Описание" });
            load.Add(new string[3] { "ОЛ", "item_COD_START", "Код КСУ" });
            load.Add(new string[3] { "ОЛ", "item_GUID", "GUID Вертикаль" });
            // Оснастка термообработки - ОТ
            load.Add(new string[3] { "ОТ", "class_NAMEVID", "Вид" });
            load.Add(new string[3] { "ОТ", "class_GUID", "GUID Вертикаль" });
            load.Add(new string[3] { "ОТ", "group_NAMETIPOSNTRM", "Тип оснастки" });
            load.Add(new string[3] { "ОТ", "group_GUID", "GUID Вертикаль" });
            load.Add(new string[3] { "ОТ", "item_NAMEOSN", "Наименование" });
            load.Add(new string[3] { "ОТ", "item_OSNAS", "Обозначение" });
            load.Add(new string[3] { "ОТ", "item_COD_START", "Код КСУ" });
            load.Add(new string[3] { "ОТ", "item_GUID", "GUID Вертикаль" });
            // Приборы
            load.Add(new string[3] { "Приборы", "class_NAME", "Вид" });
            load.Add(new string[3] { "Приборы", "class_GUID", "GUID Вертикаль" });
            load.Add(new string[3] { "Приборы", "item_NAME", "Наименование" });
            load.Add(new string[3] { "Приборы", "item_COD", "Код КСУ" });
            load.Add(new string[3] { "Приборы", "item_OBOZN", "Обозначение" });
            load.Add(new string[3] { "Приборы", "item_GUID", "GUID Вертикаль" });
            // Сборочная оснастка
            load.Add(new string[3] { "СБО", "class_A", "Вид" });
            load.Add(new string[3] { "СБО", "class_B", "Код вида" });
            load.Add(new string[3] { "СБО", "class_GUID", "GUID Вертикаль" });
            load.Add(new string[3] { "СБО", "group_OSNASGR", "Группа оснастки" });
            load.Add(new string[3] { "СБО", "group_KODTIPOSN", "Код типа оснастки" });
            load.Add(new string[3] { "СБО", "group_KODGROSN", "Код группы оснастки" });
            load.Add(new string[3] { "СБО", "group_GUID", "GUID Вертикаль" });
            load.Add(new string[3] { "СБО", "item_NAMEOSN", "Наименование" });
            load.Add(new string[3] { "СБО", "item_OSNSBR", "Обозначение" });
            load.Add(new string[3] { "СБО", "item_COD_START", "Код КСУ" });
            load.Add(new string[3] { "СБО", "item_GUID", "GUID Вертикаль" });
            // Сварочная оснастка
            load.Add(new string[3] { "СВО", "class_A", "Вид" });
            load.Add(new string[3] { "СВО", "class_B", "Код вида" });
            load.Add(new string[3] { "СВО", "class_GUID", "GUID Вертикаль" });
            load.Add(new string[3] { "СВО", "item_NAMEOSN", "Наименование" });
            load.Add(new string[3] { "СВО", "item_OSNSVR", "Обозначение" });
            load.Add(new string[3] { "СВО", "item_KODTIPOSN", "Код типа оснастки" });
            load.Add(new string[3] { "СВО", "item_KODGROSN", "Код группы оснастки" });
            load.Add(new string[3] { "СВО", "item_COD_START", "Код КСУ" });
            load.Add(new string[3] { "СВО", "item_GUID", "GUID Вертикаль" });
            // Слесарный инструмент - СИ
            load.Add(new string[3] { "СИ", "class_A", "Вид" });
            load.Add(new string[3] { "СИ", "class_GUID", "GUID Вертикаль" });
            load.Add(new string[3] { "СИ", "group_NAMEGRINS", "Группа инструмента" });
            load.Add(new string[3] { "СИ", "group_GUID", "GUID Вертикаль" });
            load.Add(new string[3] { "СИ", "group2_NAMEINSTR", "Описание инструмента" });
            load.Add(new string[3] { "СИ", "group2_INSRTSHT", "Слесарный инструмент" });
            load.Add(new string[3] { "СИ", "group2_GOST", "ГОСТ или ТУ" });
            load.Add(new string[3] { "СИ", "group2_PRIM", "Описание" });
            load.Add(new string[3] { "СИ", "group2_GUID", "GUID Вертикаль" });
            load.Add(new string[3] { "СИ", "item_OBOZN", "Обозначение" });
            load.Add(new string[3] { "СИ", "item_COD_START", "Код КСУ" });
            load.Add(new string[3] { "СИ", "item_GUID", "GUID Вертикаль" });
            return load;
        }
    }
}
